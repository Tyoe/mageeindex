<?php include("./php/_member_ch.php")?>
<!DOCTYPE>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="導遊領隊考試2014,導遊領隊考試介紹,導遊領隊歷屆試題,導遊領隊考古題,絕對考上,馬跡庫比,馬跡中心,函授,保證班,領隊執照考試,領隊證照,導遊證照">
<meta name="description" content="全國最專業領隊導遊訓練中心，領隊導遊現場課程、函授課程及考證用書，本中心課程目的，不僅為協助您取得領隊、導遊證照，其最終目的更希望能協助您實際上線帶團。我們堅持以專業的師資團隊，提供最專業的課程及輔助教材，以正確的態度及觀念傳承前輩們的經驗。並且，從中延攬優秀的領隊導遊人才加入馬跡領隊導遊派遣之行列。">
<title>馬跡導遊領隊中心</title>
</head>
<!--<link rel="stylesheet" type="text/css" href="javascript/bootstrap/css/bootstrap.min.css">-->
<link rel="stylesheet" type="text/css" href="./css/css.css"/>
<script type="text/javascript" src="./javascript/jquery-1.7.2.js"></script>
<script type="text/javascript" src="./javascript/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="./javascript/js.js"></script>
<script type="text/javascript" src="./javascript/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="./javascript/jquery.uploadify.min.js"></script>
<?php if(@$_SESSION['level']==999){echo "<script type=\"text/javascript\" src=\"javascript/admin_js.js\"></script>";}?>
<body>
<script src="./javascript/jquery-box-slider-master/js/box-slider.jquery.js"></script>
<script src="./javascript/jquery-box-slider-master/js/effects/box-slider-fx-scroll-3d.js"></script>
<script src="./javascript/jquery-box-slider-master/js/effects/box-slider-fx-fade.js"></script>
<script src="./javascript/jquery-box-slider-master/js/effects/box-slider-fx-scroll.js"></script>
<script src="./javascript/jquery-box-slider-master/js/effects/box-slider-fx-blinds.js"></script>
<script src="./javascript/jquery-box-slider-master/js/effects/box-slider-fx-carousel-3d.js"></script>
<script src="./javascript/jquery-box-slider-master/js/effects/box-slider-fx-tile-3d.js"></script> 
<?php if(@$_SESSION['level']==999){echo "<div id=\"admin_test\">
	<ul class=\"inline\">
    	<li><input type=\"button\" value=\"編輯\" id=\"edit_a\"/></li>
		<li><input type='button' value='最新消息編輯' id='edit_news'></li>
    	<li><input type=\"button\" value=\"儲存\" id=\"submit\"/></li>
        <li><input type=\"button\" value=\"Upload Image\" id=\"upfile\"/></li>
        <ll><input type=\"button\" value=\"Image\" /></li>
	</ul>
</div>";}
?>
<!-- Modal -->
<?php 

if(@$_SESSION['level']==999){
	
	
	?>
<div id="myModal" class="modal hide fade span12" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header ">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">上傳</h3>
  </div>
  <div class="modal-body">
    <p><input type="file" name='file'/></p>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    <input type="submit" class="btn btn-primary"value="Save changes"/>
  </div>
</div>
<?php }?>
<!--- 修改人:yan pin  --->
<!--- 修改程式碼 51 ~ 62 行-->
<!--原本程式碼 : 無 --->
<!--- 修改人:yan pin  --->
<style>
.Shadows_1{
		moz-box-shadow: 15px 10px 14px -13px #000 ;
		-webkit-box-shadow: 15px 10px 14px -13px #000 ;
		box-shadow: 15px 8px 12px -13px #000 ;
	}
	.Shadows_2{
		moz-box-shadow: 15px 10px 14px -13px #000 ;
		-webkit-box-shadow: 15px 10px 14px -13px #000 ;
		box-shadow: 15px 8px 12px -13px #000 ;
	}
</style>
<div id="header-box" class="Shadows_1" style="height:87px;">
<!--- 修改程式碼 67 行-->
<!-- <div id='header-box'>-->
<!--- 修改人:yan pin  --->
		<div id='header' >
        	<div id="logo_blcok" class="Shadows_2 float">
            <!--- 修改程式碼 72 行-->
            <!--<div id='logo_blcok' class="float">-->
            <!--- 修改人:yan pin  --->
            
                <div id="logo" class="float"><img src="imagesa/logo.png" height="115px" /></div> 
            </div>
            <div id="oblique" class="float" style=" margin:0px;" >
            <!--- 修改程式碼 79 行-->
            <!-- <div id='oblique'class="float">-->
            <!--- 修改人:yan pin  --->
             	<div style="background:#FFF; width:63px; height:77px;"></div>
                 <!--- 修改程式碼 83 行-->
                 <!--原本程式碼 : 無 --->
                 <!--- 修改人:yan pin  --->
                <img src="imagesa/s.png" style="width:64px; height:48px;" />
            </div>
            <div id='menu' class="float">
                <div id='menu_top'>
                    <ul class="inline text-right">
                        <li><i class="icon-home"></i><a href="http://www.gomagee.com">馬跡中心首頁</a></li>
                        <li><i class="icon-heart"></i><a href="javascript:window.external.AddFavorite('http://www.gomagee.com','馬跡中心')">加入我的最愛</a></li>
                    </ul>
                    <ul class="inline text-right " id='menu_m'>
                        <li><a href="http://www.gomagee.com/CourseOverview.php">現場課程</a></li>
                        <li><a href="http://www.gomagee.com/BooksOverview.php">考證書籍</a></li>
                        <li><A href="http://www.gomagee.com/DocsOverview.php?menu_items_id=8">領隊導遊應考須知</A></li>
                        <li><a href="http://www.gomagee.com/NavContent.php?pk=5">專人咨詢</a></li>
                        <li><a href="http://www.gomagee.com/docs.php?func_id=DOCS&pk=64">線上劃位</a></li>
                    </ul>
                </div>
            </div>
    </div>
</div>
           
<div id='advertising' style=" margin-top:86px;background:#CCC">
 <!--- 修改程式碼 106 行-->
 <!--原本程式碼 : <div id='advertising' style="background:#CCC"> --->
 <!--- 修改人:yan pin  --->
 <div id="ad-list" class="w10 mcn">
 <div id="member_sign" class="img-rounded">
 <?php include("page/member_login.php")?>
 </div>
 <div id='news_text'>
 	<?php  include("page/news_text.php")?>
 </div>
 <?php include("page/advertising.php")?>
  </div>
</div>
<div class="margin-auto magee" id="product_book_box">
  <?php include("page/product_book.php")?>
</div>
<div class="margin-auto product_back full_p" id="product_class_box">
<?php include("page/product_class.php")?>
</div>
<div class='full_p cb margin-auto' >
<div  id="footer_top" class='clear'>
    	<div class="container"><img src="imagesa/logo_f.gif" class="float"/>
        <iframe src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.gomagee.com%2F&amp;width&amp;layout=standard&amp;action=like&amp;show_faces=true&amp;share=true&amp;height=80" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:80px;" allowTransparency="true" class="float mar-t"></iframe>
        <iframe src="//www.facebook.com/plugins/follow.php?href=https%3A%2F%2Fwww.facebook.com%2Fpages%2F%25E9%25A6%25AC%25E8%25B7%25A1%25E4%25B8%25AD%25E5%25BF%2583%2F120724884648881&amp;width&amp;height=80&amp;colorscheme=light&amp;layout=standard&amp;show_faces=true" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:80px;" allowTransparency="true" class="float mar-t" ></iframe>
        </div>
    </div>
  </div>
<div id='footer' class="white full_p margin-auto">
	
  <div id="footer_counter">
    <div class="f1 mar-left product_contrl">
    <p><h3>相關課程</h3>

<a href="http://www.gomagee.com/CourseOverview.php?menu_items_id=2">三科領隊導遊證照班【台北 / 台中】</a><br />
<a href="http://www.gomagee.com/product.php?func_id=COURS&pk=152&menu_items_id=2">英語領隊導遊輔導班</a><br />
<a href="http://www.gomagee.com/product.php?func_id=COURS&pk=144">考前三科猜題衝刺班</a><br />
<a href="http://www.gomagee.com/CourseOverview.php?menu_items_id=3">英語考前衝刺班</a><br />
<a href="http://www.gomagee.com/docs.php?func_id=DOCS&pk=35">帶團實務培訓班</a><br />
    </p>
</div>
    <div class="f2 mar-left product_contrl">
   <p>
    <ul id='information_ul'>
    	<li>
        <a href="http://www.dagee.tw/qrcode.php">
         <div class='inf_icon text-center'><img src="imagesa/cord.png" /></div>
        <p class='text-center'>達跡旅行社旅<br />遊卡登入</p>
        </a>
        </li>
        <li>
        <a href="http://www.gomagee.com/DocsOverview.php?menu_items_id=8">
       <div class='inf_icon text-center'> <img src="imagesa/information_icon.png" height="68"/></div>
        <p class='text-center'>領隊導遊<br />考試資訊</p>
        </a>
        </li>
        <li>
        <a href="https://www.facebook.com/pages/%E9%A6%AC%E8%B7%A1%E4%B8%AD%E5%BF%83/120724884648881">
       <div class='inf_icon'><img src="imagesa/fb_join.png" /></div>
        <p class='text-center'>加入馬跡粉絲團<br />
隨時取得新訊！</p>
</a>
        </li>
    
    </ul>
    </p>
    </div>
    <div class="f3 mar-left product_contrl "><p>
    	<h3>考生分享專區</h3>
    	<a href="http://www.gomagee.com/docs.php?func_id=DOCS&pk=48">【聯合報報導】施雅仁 / 念3周就考上</a><br />
<a href="http://www.gomagee.com/docs.php?func_id=DOCS&pk=39">【學長姐分享】上課及考試的準備心得</a><br />
<a href="http://www.gomagee.com/docs.php?func_id=DOCS&pk=34">【快速考取證照的方法】上榜學員現身說法</a><br />
</p>
    	</div>
      
    </div>
  
</div>
<div id='copyright' class="full_p white">
	<div class='magee'>
    <div class='span3 img mar-left'><img src="imagesa/magee_icon.png"></div>
    	<div class='span9 text-center mar-left' id='magee_addr'>
        
 
        馬跡領隊導遊訓練 ‧ 派遣中心&nbsp;&nbsp;&nbsp; ADD/106 臺北市大安區復興南路二段268號3樓之1<br>
諮詢服務 / 週一至週五 10:00-18:00 &nbsp;&nbsp; <font style="color:#700">(02)2733-8118</font> &nbsp;/&nbsp; (02)7711-0050
<div style="color:#7d7b7b">馬跡庫比有限公司 版權所有 © 2014 Mageecube Company All Rights Reserved.</div>
		最佳瀏覽網頁：Google Chrome
        </div>
    
		
    </div>
</div>
</body>
</html>