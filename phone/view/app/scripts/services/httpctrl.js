'use strict';

/**
 * @ngdoc service
 * @name mageeViewApp.httpctrl
 * @description
 * # httpctrl
 * Factory in the mageeViewApp.
 */
angular.module('mageeViewApp')
  .factory('httpctrl', function ($http,API,fromgetdata) {
    var httpget = function(url, dafult) {
            dafult = (dafult) ? dafult : {};
            return $http.get(API + url + fromgetdata(dafult));
        }
        var httppost = function(url, dafult, header) {
            dafult = (dafult) ? dafult : {};
            return $http.post(API + url, dafult, header);
        }
        return {
            get: httpget,
            post: httppost
        };
  });
