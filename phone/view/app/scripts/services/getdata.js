'use strict';

/**
 * @ngdoc service
 * @name mageeViewApp.getdata
 * @description
 * # getdata
 * Factory in the mageeViewApp.
 */
angular.module('mageeViewApp')
    .factory('getdata', function(httpctrl) {
        var getnewstext = function() {
            return httpctrl.get("getnewtext")
        }
        var getDocs=function (data){
        	return httpctrl.get("getDocstext",data);
        }
        var getDocsList=function (data){
        	return httpctrl.get("getDocsList",data);
        }
        var getproClass=function (data){
        	return httpctrl.get("getproClass");
        }
        var getproBook=function (){
        	return httpctrl.get("getproBook")
        }
        var getshopcart=function (data){
            return httpctrl.get("getshopcart",{id:data});
        }
        var setOrder_log=function (data,type){
            return httpctrl.get("setOrder_log",{id:data,type:type});
        }
        var countseadmoney=function (){
            return httpctrl.get("countseadmoney");
        }
        var changeqty=function (data){
            return httpctrl.post("changeqty",{data:data});
        }
        var rmitem=function (data){
            return httpctrl.post("rmitem",{data:data});
        }
        var changedelivery=function (data){
            return httpctrl.get("changedelivery",{data:data});
        }
        var getpost_id=function (data){
            return httpctrl.get("getpost_id");
        }
        var setOrder=function (data){
            return httpctrl.post("setOrder",data);
        }
        var crlogin=function (data){
            return httpctrl.post("login",data);
        }
        var getNavcontent=function (data){
            return httpctrl.get("getNavcontent");
        }
        var gethotnews=function (data){
            return httpctrl.get("gethotnews");
        }
        var registered=function (data){
            return httpctrl.post("registered",data);
        }
        var getpro_information=function (data){
            return httpctrl.get("get_information",data);
        }
        return {
            getnewstext: getnewstext,
            getDocs:getDocs,
            getDocsList:getDocsList,
            getproClass:getproClass,
            getproBook:getproBook,
            countseadmoney:countseadmoney,
            getshopcart:getshopcart,
            setOrder_log:setOrder_log,
            changeqty:changeqty,
            rmitem:rmitem,
            changedelivery:changedelivery,
            getpost_id:getpost_id,
            setOrder:setOrder,
            crlogin:crlogin,
            getNavcontent:getNavcontent,
            gethotnews:gethotnews,
            registered:registered,
            getpro_information:getpro_information
        }
    });
