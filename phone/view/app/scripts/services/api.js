'use strict';

/**
 * @ngdoc service
 * @name mageeViewApp.API
 * @description
 * # API
 * Constant in the mageeViewApp.
 */
// var apiurl = "http://localhost/mageeindex/phone/api/public/api/";
// var inlcudeurl = "http://localhost/mageeindex/phone/";
var apiurl = "http://www.magee.tw/mobile/api/public/api/";
var inlcudeurl = "http://www.magee.tw/";
var imageurl = "http://www.magee.tw/BOOKS/";


angular.module('mageeViewApp')
    .constant('API', apiurl).constant('imageAPI', imageurl).constant("beforepage", "").constant("inlcudeurl",inlcudeurl).constant("fromgetdata", function(objectdata) {
        var tmp = [];
        angular.forEach(objectdata,
            function(element, index) {
                tmp.push(index + "=" + element);
            });
        return "?" + tmp.join("&");
    });
