'use strict';

/**
 * @ngdoc filter
 * @name mageeViewApp.filter:tranIfa
 * @function
 * @description
 * # tranIfa
 * Filter in the mageeViewApp.
 */
angular.module('mageeViewApp')
  .filter('tranIfa', function ($sce) {
    return function (input) {

      return $sce.trustAsHtml(input);
    };
  });
