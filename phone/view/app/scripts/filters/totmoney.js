'use strict';

/**
 * @ngdoc filter
 * @name mageeViewApp.filter:totmoney
 * @function
 * @description
 * # totmoney
 * Filter in the mageeViewApp.
 */
angular.module('mageeViewApp')
    .filter('totmoney', function() {
        return function(input, attr) {
            var kg = { money: 0, count: 0 }

            $.each(input, function(keys, item) {
                kg.money += item.price * item.qty;
                kg.count += item.qty;
            });
            return kg[attr];
        };
    });
