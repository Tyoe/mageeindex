'use strict';

/**
 * @ngdoc filter
 * @name mageeViewApp.filter:tranhtml
 * @function
 * @description
 * # tranhtml
 * Filter in the mageeViewApp.
 */
angular.module('mageeViewApp')
    .filter('tranhtml', function() {
        return function(input) {
            if (input) {
                input = input.replace(/\r/g, "<br/>");
            }
            return input;
        };
    });
