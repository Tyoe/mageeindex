'use strict';

/**
 * @ngdoc directive
 * @name mageeViewApp.directive:changeToComputer
 * @description
 * # changeToComputer
 */
angular.module('mageeViewApp')
    .directive('changeToComputer', function($location) {
        return {
            restrict: 'AE',
            link: function postLink(scope, element, attrs) {
                element.on("click", function() {
                	sessionStorage.newmode='computer';
                	window.location='http://www.magee.tw'
                });
            }
        };
    });
