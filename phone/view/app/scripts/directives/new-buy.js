'use strict';

/**
 * @ngdoc directive
 * @name mageeViewApp.directive:newBuy
 * @description
 * # newBuy
 */
angular.module('mageeViewApp')
    .directive('newBuy', function() {
        return {
            restrict: 'AE',
            controller: "NewBuyCtrl",
            controllerAs: "NBC",
            scope: { "newBuy": "=","intype" :"="},
            link: function postLink(scope, element, attrs) {
                element.on("click", function() {
                    // console.log(scope.intype);
                     scope.NBC.nowbuy(scope.newBuy,scope.intype);
                })
            }
        };
    });
