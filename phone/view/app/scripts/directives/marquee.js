'use strict';

/**
 * @ngdoc directive
 * @name mageeViewApp.directive:Marquee
 * @description
 * # Marquee
 */
angular.module('mageeViewApp')
    .directive('marquee', function($timeout) {
    	var changeitem=function (){
    		$(".Marquee .Marquee-box").animate({
    			"margin-top":-$(".Marquee .Marquee-item").eq(0).height()-5
    		},1000,function (){
    			 $(".Marquee .Marquee-box").append($(".Marquee .Marquee-item").eq(0)).removeAttr('style');
    			 $timeout(changeitem,3000);
    		});
    	}
        return {
            templateUrl: 'views/page/marquee.html',
            restrict: 'EA',
            scope: { mardata: "=" },
            controller: function($scope) {
                var vm = this;
                vm.sel=function (){
                	vm.hotnews =$scope.mardata;
                }
                

            },
            controllerAs: "Mar",
            link: function postLink(scope, element, attrs) {
                scope.$watch(function() {
                    return scope.mardata;
                }, function(data) {
                	// console.log(scope);
                    if (data.length>0) {
                    	scope.Mar.sel();
                       $timeout(changeitem,3000);
                    }
                });

            }
        };
    });
