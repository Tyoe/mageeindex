'use strict';

/**
 * @ngdoc directive
 * @name mageeViewApp.directive:intocart
 * @description
 * # intocart
 */
angular.module('mageeViewApp')
    .directive('intocart', function(httpctrl) {
        return {
            restrict: 'AE',
            scope: { "intocart": "=" ,"intype":"="},
            controller: "IntocartCtrl",
            controllerAs: "IC",
            link: function postLink(scope, element, attrs) {
                element.on("click", function() {
                	console.log(scope.intocart);
                    scope.IC.incart(scope.intocart,scope.intype);
                    $(".incartshow").fadeIn(500, function() {
                        var th = $(this);
                        setTimeout(function() {
                            th.fadeOut(500);
                        }, 2000);
                    });

                })
            }
        };
    });
