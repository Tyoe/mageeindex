'use strict';

/**
 * @ngdoc directive
 * @name mageeViewApp.directive:img
 * @description
 * # img
 */
angular.module('mageeViewApp')
    .directive('img', function() {
        return {
            restrict: 'E',
            link: function postLink(scope, element, attrs) {
                element.bind("error", function() {
                    console.log("dsf");

                    $(this).attr("src", "http://www.magee.tw/web/product/item/default.jpg");
                    // img.onload = function() {
                    // 	console.log("SDf");
                    //     // image  has been loaded
                    // };
                    // img.onerror=function (){
                    // 	console.log("error");
                    // }

                });
                element.bind("load", function() {
                    console.log($(this).attr("src"));
                });
            }
        };
    });
