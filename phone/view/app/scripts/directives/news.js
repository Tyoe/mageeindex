'use strict';

/**
 * @ngdoc directive
 * @name mageeViewApp.directive:news
 * @description
 * # news
 */
angular.module('mageeViewApp')
  .directive('newsBox', function () {
    return {
      templateUrl: 'views/news.html',
      restrict: 'EA',
      controller:"NewsCtrl",
      controllerAs:"NC",
      replace:true,
      link: function postLink(scope, element, attrs) {
      }
    };
  });
