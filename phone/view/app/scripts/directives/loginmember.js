'use strict';

/**
 * @ngdoc directive
 * @name mageeViewApp.directive:loginmember
 * @description
 * # loginmember
 */
angular.module('mageeViewApp')
    .directive('loginmember', function($uibModal) {
        return {
            restrict: 'EA',
            link: function postLink(scope, element, attrs) {
                element.on("click", function() {
                    var login = $uibModal.open({
                        ariaLabelledBy: 'modal-title',
                        ariaDescribedBy: 'modal-body',
                        templateUrl: 'views/loginmember.html',
                        controller: 'LoginmemberCtrl',
                        controllerAs: 'LBC',
                        size: "lg",
                    });
                    login.result.then(function (data){
                    	

                    });
                })
            }
        };
    });
