'use strict';

/**
 * @ngdoc directive
 * @name mageeViewApp.directive:goTop
 * @description
 * # goTop
 */
angular.module('mageeViewApp')
    .directive('goTop', function($window) {
        return {
            restrict: 'A',
            link: function postLink(scope, element, attrs) {
                element.on("click", function() {
                    $("html,body").animate({ "scrollTop": 0 }, 500);
                })

            }
        };
    });
