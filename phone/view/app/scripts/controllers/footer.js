'use strict';

/**
 * @ngdoc function
 * @name mageeViewApp.controller:FooterCtrl
 * @description
 * # FooterCtrl
 * Controller of the mageeViewApp
 */
angular.module('mageeViewApp')
  .controller('FooterCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
