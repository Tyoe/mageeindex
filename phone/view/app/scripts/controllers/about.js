'use strict';

/**
 * @ngdoc function
 * @name mageeViewApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the mageeViewApp
 */
angular.module('mageeViewApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
