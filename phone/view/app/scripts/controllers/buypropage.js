'use strict';

/**
 * @ngdoc function
 * @name mageeViewApp.controller:BuypropageCtrl
 * @description
 * # BuypropageCtrl
 * Controller of the mageeViewApp
 */
angular.module('mageeViewApp')
    .controller('BuypropageCtrl', function(getdata, $scope, $state, beforepage, $rootScope) {
        var vm = this;
        vm.active = 0;
        vm.delcname = ['郵局', "自取"];
        // var localstrong = JSON.parse(localStorage.shopchart);

        $scope.$watch(function() {
            return $rootScope.member
        }, function(data) {
            if (data) {
                var member = data;
                vm.member = member;
                vm.ordermember = { name: member.member_cname, phone_no: member.phone_no, mobil_no: member.mobil_no, email: member.email, post_id: member.post_id, address: member.address }
            }
        });
        getdata.getshopcart().then(function(data) {
            vm.incart = data.data;
            vm.radiodelivery = data.data[0].delivery;
        });
        getdata.getpost_id().then(function(data) {
            vm.post_id = data.data;
        });
        var changdelivery = function(item) {
            if (item == 0) {
                getdata.countseadmoney().then(function(data) {
                    vm.delivery = data.data.deliery;
                });
            } else {
                vm.delivery = 0;
            }
        }
        $scope.$watch(function() {
            return vm.radiodelivery;
        }, function(data) {
            if (data) {
                getdata.changedelivery(data).then(function(data) { console.log(data) });
                changdelivery(data);
            }
        })
        vm.changeqty = function(data) {
            getdata.changeqty(data).then(function(data) {
                changdelivery(vm.radiodelivery);
            });
        }
        vm.rmitem = function(data) {
            var tk = data;
            getdata.rmitem(data).then(function(data) {
                vm.incart = data.data;
                changdelivery(vm.radiodelivery);
            })
        }
        vm.goback = function() {
            if ($rootScope.before.name) {
                $state.go($rootScope.before.name);
            } else {
                $state.go("Base");
            }
        }
        vm.nextpage = function() {
            vm.active++;
        }
        vm.uppage = function() {
            vm.active--;
        }
        vm.ordermember = { "name": "", "phone_no": "", "mobil_no": "", "post_id": "", "address": "", "email": "" };
        vm.delivermember = { "name": "", "phone_no": "", "mobil_no": "", "post_id": "", "address": "" };
        var field = { "name": "中文全名", "phone_no": "聯絡電話", "mobil_no": "手機", "post_id": "地區", "address": "地址", "email": "電子郵件" };
        var error = [];
        $scope.$watch(function() {
            return vm.active;
        }, function(data) {
            if (data == 3) {
                error=[];
                $.each(vm.ordermember, function(keys, item) {
                    if (item == "" && keys.search(/inv_/g) < 0) {
                        error.push("訂購資料" + field[keys] + "請勿留空白");
                    }
                });
                $.each(vm.delivermember, function(keys, item) {
                    if (item == "" && keys.search(/inv_/g) < 0) {
                        error.push("收件資料" + field[keys] + "請勿留空白");
                    }
                });
                if (error.length > 0) {
                    alert(error.join("\n"));
                    vm.active--;
                }
            }
        })
        vm.submitorder = function() {
            if(error.length>0){
                alert("訂購資料與收件人資料有錯誤");
                return ;
            }
            var ordermember = vm.ordermember;
            var delivermember = vm.delivermember;
            var buylist = [];
            $.each(vm.incart, function(keys, item) {
                buylist.push({ pk: item.pk });
            });
            getdata.setOrder({ ordermember: ordermember, delivermember: delivermember, buylist: buylist, delivermoney: vm.delivery, "memberuid": vm.member.member_id }).then(
                function(data) {
                    console.log(data);
                    if (data.data.state == "success") {
                        sessionStorage.orderid = data.data.uid;
                        sessionStorage.delivermoney = data.data.delivermoney;
                        sessionStorage.ordermember = JSON.stringify(data.data.ordermember);
                        sessionStorage.delivermember = JSON.stringify(data.data.delivermember);
                        sessionStorage.order_cnt = JSON.stringify(data.data.order_cnt);
                        sessionStorage.post_id = JSON.stringify(data.data.post_id);
                        $state.go("Base.OrderFinal");
                    } else if (data.data.state == "error") {
                        alert("訂單新增錯誤，請您再輸入一次");
                    }
                })
        }
    });
