'use strict';

/**
 * @ngdoc function
 * @name mageeViewApp.controller:OrderfinalCtrl
 * @description
 * # OrderfinalCtrl
 * Controller of the mageeViewApp
 */
angular.module('mageeViewApp')
    .controller('OrderfinalCtrl', function($filter) {
        var vm = this;
        var orderid = sessionStorage.orderid;
        var delivermoney = sessionStorage.delivermoney;
        var ordermember = JSON.parse(sessionStorage.ordermember);
        var delivermember = JSON.parse(sessionStorage.delivermember);
        var order_cnt = JSON.parse(sessionStorage.order_cnt);
        var post_id = JSON.parse(sessionStorage.post_id);
        vm.orderfinal = { orderid: orderid, delivermoney: delivermoney, ordermember: ordermember, delivermember: delivermember, order_cnt: order_cnt };
        vm.orderfinal.ordermember.post_id_cname = $filter("filter")(post_id, { post_id: ordermember.post_id })[0];
        vm.orderfinal.delivermember.post_id_cname = $filter("filter")(post_id, { post_id: delivermember.post_id })[0];
        vm.moneytotel = 0;
        (vm.orderfinal.order_cnt).map(function(items, keys) {
            vm.moneytotel += items.qty * items.price;
        });
        sessionStorage.clear();
    });
