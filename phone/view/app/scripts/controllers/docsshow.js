'use strict';

/**
 * @ngdoc function
 * @name mageeViewApp.controller:DocsshowCtrl
 * @description
 * # DocsshowCtrl
 * Controller of the mageeViewApp
 */
angular.module('mageeViewApp')
  .controller('DocsshowCtrl', function ($stateParams,getdata,$sce) {
  	var vm=this;
  	var Class=$stateParams.Class;
  	var id=$stateParams.id;
  	getdata.getDocs({Class:Class,id:id}).then(function (data){
  		data.data.content=(data.data.content).replace(/&quot;/g,"\"");
  		vm.data=data.data;
  	})
  });
