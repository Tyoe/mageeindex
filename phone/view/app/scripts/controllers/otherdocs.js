'use strict';

/**
 * @ngdoc function
 * @name mageeViewApp.controller:OtherdocsCtrl
 * @description
 * # OtherdocsCtrl
 * Controller of the mageeViewApp
 */
angular.module('mageeViewApp')
  .controller('OtherdocsCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
