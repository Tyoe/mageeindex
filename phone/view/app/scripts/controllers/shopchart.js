'use strict';

/**
 * @ngdoc function
 * @name mageeViewApp.controller:ShopchartCtrl
 * @description
 * # ShopchartCtrl
 * Controller of the mageeViewApp
 */
angular.module('mageeViewApp')
  .controller('ShopchartCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
