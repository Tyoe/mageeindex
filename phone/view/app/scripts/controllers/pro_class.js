'use strict';

/**
 * @ngdoc function
 * @name mageeViewApp.controller:ProClassCtrl
 * @description
 * # ProClassCtrl
 * Controller of the mageeViewApp
 */
angular.module('mageeViewApp')
    .controller('ProClassCtrl', function(getdata, imageAPI, $scope, $state) {
        $('.btnopen').on('click',function(){

            var nowheight=$('#scheme').height();
            var autoheight=$('#scheme').css('height', 'auto').height();
            if ( $('.btnopen').hasClass('click') ) {
                $('.btnopen').removeClass('click');
                $('#scheme').animate({height: '150px'}, 500);
                $("html,body").animate({ "scrollTop": 0 }, 500);
            } else {
                $('.btnopen').addClass('click');
                $('#scheme').height(nowheight).animate({height: autoheight}, 500);
            }
        });
        var vm = this;
        vm.ka = null;
        vm.prolist = [];
        if (sessionStorage.class) {
            var dafult=JSON.parse(sessionStorage["class"]);
            vm.overview = dafult.overview;
            vm.scheme_name = dafult.scheme_name;
            vm.web_head = dafult.web_head;
        }
        getdata.getproClass().then(function(data) {
            data.data.overview = data.data.overview.map(function(item, keys) {
                item.s_img = imageAPI + item.s_img;
                return item;
            });
            vm.overview = data.data.overview;
            vm.scheme_name = data.data.scheme_name;
            vm.web_head = data.data.web_head;
            sessionStorage["class"] = JSON.stringify(data.data);
        });
        vm.proinfodafult = function(data) {
            // console.log(data);
            return data[Object.keys(data)[0]][0];
        }
        vm.openitem = function(data, a) {
                vm.ka = (vm.ka == data) ? "all" : data;

                $(".saall").addClass("hide");
                if (vm.ka != "all") {
                    $("body").animate({ scrollTop: $("#a" + a).offset().top - 80 }, "slow");
                    $(".s" + a).removeClass("hide");
                }

            }
            // $scope.$watch(function() {
            //     return localStorage.shopchart;
            // }, function(data) {
            //     // console.log(JSON.parse(data)['COURS']);
            //     vm.incartlocal = JSON.parse(data)['COURS'];
            // });
    });
