'use strict';

/**
 * @ngdoc function
 * @name mageeViewApp.controller:DocspageCtrl
 * @description
 * # DocspageCtrl
 * Controller of the mageeViewApp
 */
angular.module('mageeViewApp')
    .controller('DocspageCtrl', function(getdata, $stateParams,$anchorScroll,$location) {
    	var vm=this;
        var dpt = $stateParams.dpt;
        getdata.getDocsList({Class:dpt}).then(function (data){
        	vm.DPT=data.data[1];
        	vm.Doclist=data.data[0];
        });
        vm.goto=function (data){
        	$("body").animate({scrollTop: $("#"+data).offset().top-80}, "slow");

        }
    });
