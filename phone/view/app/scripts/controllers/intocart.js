'use strict';

/**
 * @ngdoc function
 * @name mageeViewApp.controller:IntocartCtrl
 * @description
 * # IntocartCtrl
 * Controller of the mageeViewApp
 */
angular.module('mageeViewApp')
    .controller('IntocartCtrl', function($state, $scope, getdata) {
        var vm = this;
        vm.incart = function(data, type) {
            // var sk = (localStorage['shopchart']) ? JSON.parse(localStorage['shopchart']) : { "BOOKS": [], "COURS": [] };
            // if (sk[type].indexOf(data) == -1) {
            //     sk[type].push(data);
            //     localStorage['shopchart'] = JSON.stringify(sk);
            // }
            getdata.setOrder_log(data, type).then(function(data) {console.log(data)});
        }

    });
