'use strict';

/**
 * @ngdoc function
 * @name mageeViewApp.controller:PageFreelistenCtrl
 * @description
 * # PageFreelistenCtrl
 * Controller of the mageeViewApp
 */
angular.module('mageeViewApp')
  .controller('PageFreelistenCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
