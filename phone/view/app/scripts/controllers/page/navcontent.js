'use strict';

/**
 * @ngdoc function
 * @name mageeViewApp.controller:PageNavcontentCtrl
 * @description
 * # PageNavcontentCtrl
 * Controller of the mageeViewApp
 */
angular.module('mageeViewApp')
  .controller('PageNavcontentCtrl', function (getdata) {
   var vm=this;
   getdata.getNavcontent().then(function (data){
   	vm.content=data.data.replace(/&quot;/g,"\"");
   });
  });
