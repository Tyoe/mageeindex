'use strict';

/**
 * @ngdoc function
 * @name mageeViewApp.controller:PageStudentshareCtrl
 * @description
 * # PageStudentshareCtrl
 * Controller of the mageeViewApp
 */
angular.module('mageeViewApp')
  .controller('PageStudentshareCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
