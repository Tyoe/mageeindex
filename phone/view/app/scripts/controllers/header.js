'use strict';

/**
 * @ngdoc function
 * @name mageeViewApp.controller:HeaderCtrl
 * @description
 * # HeaderCtrl
 * Controller of the mageeViewApp
 */
angular.module('mageeViewApp')
    .controller('HeaderCtrl', function($scope, $window, $rootScope) {
        var vm = this;
        vm.topmenushow = false;
        vm.sko = "S";
        vm.active=false;

        vm.logout = function() {
            localStorage.member = null;
        }
       
        $scope.$watch(function() {
            return $rootScope.member
        }, function(data) {
            vm.ka=data;
        });
        $($window).bind("scroll", function() {
            if (this.scrollY > 100) {
                vm.topmenushow = true;
                $rootScope.goTopshow=true;
            } else {
                vm.topmenushow = false;
                $rootScope.goTopshow=false;
            }
            $scope.$apply();
        });

    });
