'use strict';

/**
 * @ngdoc function
 * @name mageeViewApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the mageeViewApp
 */
// var sht = "http://localhost/mageebox/phone/";


function fadechange(number, nextnumber) {
    $("#ad_change").find("li").eq(number).fadeOut(500);
    $("#ad_change").find("li").eq(nextnumber).fadeIn(500);
}



angular.module('mageeViewApp')
    .controller('MainCtrl', function($http, $timeout, getdata, inlcudeurl) {
        var timeout = function(start, next, totel) {
            next = start + 1;
            if (next == totel) {
                next = 0;
            }
            fadechange(start, next);
            start = next;
            $timeout(function() {
                timeout(start, next, totel);
            }, 5000);
        }

        var vm = this;
        vm.Marquee = [];
        getdata.gethotnews().then(function(data) {
            vm.Marquee = data.data;
        });
        vm.swipe = { "book": 0, "class": 0 };
        vm.booklenght = 0;
        vm.freeclass = 3
        vm.rightlight = function(data, swipe, type, ele) {
            if (swipe[type] < $(data).length - 1) {
                swipe[type]++;
                var x = $(data).eq(swipe[type]).offset().left + $(ele)[0].scrollLeft;
                // var nx=$(data).eq(swipe[type]-1).offset().left;

                $(ele).animate({
                    "scrollLeft": x
                }, 500);
            }
        }
        vm.leftlight = function(data, swipe, type, ele) {
            if (swipe[type] > 0) {
                swipe[type]--;
                var x = $(data).eq(swipe[type]).offset().left + $(ele)[0].scrollLeft;
                // var nx=$(data).eq(swipe[type]-1).offset().left;

                $(ele).animate({
                    "scrollLeft": x
                }, 500);
            }
        }
        $http.get(inlcudeurl + 'page/advertising.php').then(function(data) {
            $("#ad").html(data.data);
            $("#ad").find("img").each(function(keys, item) {
                $("#ad").find("img").eq(keys).attr("src", $(item).attr("src").replace(/\.\//g, "http://www.magee.tw/"));
                $("#ad").find("img").eq(keys).removeAttr('height');
            });
            // vm.style={width:$("#ad").find("img").height()};
            $("ul#ad_change").css("min-height", 168);
            var start = 0;
            var next = 0;
            var totel = $("#ad_change").find("li").length;
            $timeout(function() {
                timeout(start, next, totel);
            }, 5000);
        });
        $http.get(inlcudeurl + 'page/product_book.php').then(function(data) {
            $("#product_booka").html(data.data);
            $("#product_book").find("img").each(function(keys, item) {
                $("#product_book").find("img").eq(keys).attr("src", $(item).attr("src").replace(/\.\//g, "http://www.magee.tw/"));
                $("#product_book").find("img").eq(keys).removeAttr('height');
            });
            vm.booklenght = $(".product_b").length;
            $("#product_booka").parents(".ckswipt").find(".upleft,.nextright").css({ "height": $(".product_b").height() + 20, "padding-top": ($(".product_b").height() + 20) / 2 });
            // $("#product_book").height($(".product_b").height()+50);
            // $(".product_b").hide();
            var width = 0;

            $("#product_book").width(400 * 4);

        })
        $http.get(inlcudeurl + 'page/product_class.php').then(function(data) {
            $("#product_classa").html(data.data);
            $("#product_classa").find("img").each(function(keys, item) {
                $("#product_classa").find("img").eq(keys).attr("src", $(item).attr("src").replace(/\.\//g, "http://www.magee.tw/"));
                $("#product_classa").find("img").eq(keys).removeAttr('height');
            });
            $("#product_classa").parents(".ckswipt").find(".upleft,.nextright").css({ "height": $(".product_l").height() + 20, "padding-top": ($(".product_l").height() + 20) / 2 });
            // $("#product_book").height($(".product_b").height()+50);
            // $(".product_b").hide();
            var width = 0;
            $("#product_class").width(400 * 3);
        })
        vm.changecomputer = function() {
            console.log("Sdf");
        }
    });
