'use strict';

/**
 * @ngdoc function
 * @name mageeViewApp.controller:NewsCtrl
 * @description
 * # NewsCtrl
 * Controller of the mageeViewApp
 */
angular.module('mageeViewApp')
    .controller('NewsCtrl', function(getdata) {
        var vm = this;
        getdata.getnewstext().then(function(data) {
            vm.newstext = (data.data).map(function(item, keys) {
                return item;
            });
            // vm.newstext=data.data;
        });
    });
