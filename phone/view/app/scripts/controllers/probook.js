'use strict';

/**
 * @ngdoc function
 * @name mageeViewApp.controller:ProbookCtrl
 * @description
 * # ProbookCtrl
 * Controller of the mageeViewApp
 */
angular.module('mageeViewApp')
    .controller('ProbookCtrl', function(getdata, imageAPI) {
        var vm = this;
        vm.ka = null;
        vm.prolist = [{ name: "ss" }, { name: "kk" }];
        if (sessionStorage.book) {
            var dafult = JSON.parse(sessionStorage["book"]);
             vm.prolist=dafult;
        }
        getdata.getproBook().then(function(data) {
            data.data = (data.data).map(function(item, keys) {
                item.descrip = (item.descrip).replace(/\r/g, "<br />");
                item.s_img = imageAPI + item.s_img;
                return item;
            });
            vm.prolist = data.data;
            sessionStorage["book"] = JSON.stringify(data.data);
        });
        vm.openitem = function(data) {
            vm.ka = (vm.ka == data) ? "all" : data;
            $(".saall").addClass("hide");
            if (vm.ka != "all") {
                $("body").animate({ scrollTop: $("#a" + data).offset().top - 80 }, "slow");
                $(".ss" + data).removeClass("hide");
            }

        }
    });
