'use strict';

/**
 * @ngdoc function
 * @name mageeViewApp.controller:LoginmemberCtrl
 * @description
 * # LoginmemberCtrl
 * Controller of the mageeViewApp
 */
angular.module('mageeViewApp')
    .controller('LoginmemberCtrl', function($uibModalInstance, getdata) {
        var vm = this;
        vm.sgin = { member_id: "", "password": "", member_cname: "", phone_no: "", mobil_no: "", birth_date: "", post_id: "", address: "" };
        vm.login = function() {
            var account = vm.account;
            var password = vm.password;
            if (account == "" || password == "") {} else {
                getdata.crlogin({ account: account, password: password }).then(function(data) {

                    if (data.data[0]) {
                        localStorage.member = JSON.stringify(data.data[0]);
                        $uibModalInstance.close({ member: data.data[0] });
                    } else {
                        alert("帳號密碼輸入錯誤，請再確認！");
                    }
                    // localStorage.member = JSON.stringify(data.data[0]);
                    // $uibModalInstance.close({ member: data.data[0] });
                });
            }
        }
        vm.loadpost = function() {
            getdata.getpost_id().then(function(data) {
                console.log(data);
                vm.post_id = data.data;
            });
        }
        vm.sgina = function() {
            console.log(vm.sgin);
            vm.error = [];
            $.each(vm.sgin, function(keys, item) {
                if (!item || item == "") {
                    vm.error.push(keys);
                }
            });
            if (vm.error.length > 0) {
                alert("有欄位空白，請再檢查！");
                return;
            }
            if (vm.check.password != vm.sgin.password) {
                vm.error.push("ckpassword");
                alert("請再輸入一次密碼");

            }
            var pattern = /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(\.[a-zA-Z0-9_-])+/; 
            if(!pattern.test(vm.sgin.member_id)){
                vm.error.push("member_id");
                alert("帳號請輸入常用email");
                return ;
            }
            if (vm.agree) {
                vm.sgin.email=vm.sgin.member_id;
                getdata.registered({member:vm.sgin}).then(function (data){
                    console.log(data);
                    if(data.data.state=='double'){
                        alert("帳號已被註冊過,請更換帳號");
                    }else if(data.data.state=='newjoin'){
                        alert("註冊成功，請至信箱收信");
                        vm.member_sign=false;
                        vm.nowpagecnamge='會員登入';
                        vm.account=data.data.email;
                        vm.password="";
                    };
                })
            }else{
                alert("請查閱網站會約定");
            }
        }
    });
