'use strict';

/**
 * @ngdoc function
 * @name mageeViewApp.controller:ProductlistCtrl
 * @description
 * # ProductlistCtrl
 * Controller of the mageeViewApp
 */
angular.module('mageeViewApp')
  .controller('ProductlistCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
