'use strict';

/**
 * @ngdoc function
 * @name mageeViewApp.controller:NewBuyCtrl
 * @description
 * # NewBuyCtrl
 * Controller of the mageeViewApp
 */
angular.module('mageeViewApp')
    .controller('NewBuyCtrl', function($state, getdata) {
        var vm = this;
        vm.nowbuy = function(data, type) {
            var sk = (localStorage['shopchart']&&localStorage['shopchart']!="null") ? JSON.parse(localStorage['shopchart']) : { "BOOKS": [], "COURS": [] };
            if (sk[type].indexOf(data) == -1) {
                sk[type].push(data);
                localStorage['shopchart'] = JSON.stringify(sk);
            }
            getdata.setOrder_log(data, type).then(function(data) {
                $state.go("Base.buypro");
            });
        }
        
    });
