'use strict';

/**
 * @ngdoc function
 * @name mageeViewApp.controller:ProInformationCtrl
 * @description
 * # ProInformationCtrl
 * Controller of the mageeViewApp
 */
angular.module('mageeViewApp')
    .controller('ProInformationCtrl', function($stateParams, getdata, imageAPI, $state) {
        var vm = this;
        var func_id = $stateParams.func_id;
        var id = $stateParams.id;
        vm.cname = func_id.toLowerCase();
        if (func_id == 'BOOKS') {
            vm.gopagename="書籍商品"
        } else {
            vm.gopagename="課程商品"
        }
        vm.gopage = function() {
            if (func_id == 'BOOKS') {
                $state.go("Base.ProBook");
            } else {
                $state.go("Base.ProClass");
            }
        }
        getdata.getpro_information({ func_id: func_id, id: id }).then(function(data) {
            console.log(data);
            data.data = data.data[0];
            data.data.content = (data.data.content).replace(/&quot;/g, "\"");
            data.data.descrip = (data.data.descrip).replace(/&quot;/g, "\"");
            data.data.b_img = imageAPI + data.data.b_img;
            vm.information = data.data;

        }, function(data) {
            alert("Ajax error");
        });
    });
