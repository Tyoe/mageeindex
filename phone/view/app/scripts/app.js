'use strict';

/**
 * @ngdoc overview
 * @name mageeViewApp
 * @description
 * # mageeViewApp
 *
 * Main module of the application.
 */
angular
    .module('mageeViewApp', [
        'ngAnimate',
        'ngCookies',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ngTouch',
        'ui.router',
        'satellizer',
        'ui.bootstrap'
    ])
    .config(function($stateProvider, $urlRouterProvider, $authProvider, $httpProvider, $locationProvider, API, $sceDelegateProvider, $sceProvider) {
        // $locationProvider.html5Mode(true);
        $urlRouterProvider.otherwise('/index');
        $stateProvider.state("Base", {
            url: '/index',
            controller: 'MainCtrl',
            controllerAs: 'main',
            views: {
                "": {

                    templateUrl: 'views/base.html'
                },
                "header": {
                    controller: 'HeaderCtrl',
                    controllerAs: 'header',
                    templateUrl: 'views/header.html'
                },
                "footer": {
                    templateUrl: 'views/footer.html'
                },
                "body@Base": {
                    controller: 'MainCtrl',
                    controllerAs: 'main',
                    templateUrl: 'views/main.html'
                }
            }
        }).state("Base.ProBook", {
            url: "/Book/",
            views: {
                "body": {
                    controller: "ProbookCtrl",
                    controllerAs: "PBC",
                    templateUrl: 'views/probook.html'
                }
            }
        }).state("Base.ProClass", {
            url: "/Class/",
            views: {
                "body": {
                    controller: "ProClassCtrl",
                    controllerAs: "PCC",
                    templateUrl: 'views/pro_class.html'
                }
            }
        }).state("Base.Docs", {
            url: "/Docs/:dpt",
            views: {
                "body": {
                    controller: "DocspageCtrl",
                    controllerAs: "DP",
                    templateUrl: 'views/docspage.html'
                }
            }
        }).state("Base.DocsShow", {
            url: "/DocsShow/:Class/:id",
            views: {
                "body": {
                    controller: "DocsshowCtrl",
                    controllerAs: "DC",
                    templateUrl: 'views/docsshow.html'
                }
            }
        }).state("Base.ShopCart", {
            url: "/ShopCart/",
            views: {
                "body": {
                    controller: "ShopchartCtrl",
                    controllerAs: "SCC",
                    templateUrl: 'views/shopchart.html'
                }
            }
        }).state("Base.buypro", {
            url: "/buypro/",
            views: {
                "body": {
                    controller: "BuypropageCtrl",
                    controllerAs: "BPC",
                    templateUrl: 'views/buypropage.html'
                }
            }
        }).state("Base.OrderFinal", {
            url: "/OrderFinal/",
            views: {
                "body": {
                    controller: "OrderfinalCtrl",
                    controllerAs: "OFC",
                    templateUrl: 'views/orderfinal.html'
                }
            }
        }).state("Base.freelisten", {
            url: "/freelisten/",
            views: {
                "body": {
                    controller: "PageFreelistenCtrl",
                    controllerAs: "PFC",
                    templateUrl: 'views/freelisten.html'
                }
            }
        }).state("Base.navcontent", {
            url: "/navcontent/",
            views: {
                "body": {
                    controller: "PageNavcontentCtrl",
                    controllerAs: "PNC",
                    templateUrl: 'views/page/navcontent.html'
                }
            }
        }).state("Base.studentshare", {
            url: "/studentshare/",
            views: {
                "body": {
                    controller: "PageStudentshareCtrl",
                    controllerAs: "PSC",
                    templateUrl: 'views/studentshare.html'
                }
            }
        }).state("Base.pro_information", {
            url: "/pro_information/:func_id/:id",
            views: {
                "body": {
                    controller: "ProInformationCtrl",
                    controllerAs: "PIC",
                    templateUrl: 'views/pro-information.html'
                }
            }
        });
        API = (API.search(/http/g) > -1) ? API : "http://www.magee.tw/newErp/" + API;
        $authProvider.loginUrl = API + "login";
        $authProvider.tokenName = 'api_token';
        $sceDelegateProvider.resourceUrlWhitelist([
            // Allow same origin resource loads.
            'self',
            // Allow loading from our assets domain.  Notice the difference between * and **.
            '**'
        ]);
        $sceProvider.enabled(true);
        $httpProvider.defaults.useXDomain = true;
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
    }).run(function($rootScope, $state, $auth, $http, beforepage, $window) {
        $rootScope.states = {};
        // $rootScope.$watch(function (){
        //   return $window.scrollY;
        // },function (data){
        //   console.log(data);
        // });
        sessionStorage.newmode = 'computer';
        window.location = 'http://www.magee.tw/index.php';

        console.log($rootScope);
        $rootScope.closemenu = function() {
            console.log($rootScope);
            $rootScope.header.active = false;
        }
        $rootScope.$watch(function() {
            return localStorage.member;
        }, function(data) {
            if (data) {
                $rootScope.member = JSON.parse(localStorage.member);
                // console.log($rootScope);
            } else {
                $rootScope.member = "";
            }
        });
        // 提供抽象state的child state可以使用抽象state來active item
        // e.g. states[抽象state名稱]; ex. 請見settings.html
        function updateStates() {
            angular.forEach($state.get(), function(state) {
                $rootScope.states[state.name] = $state.includes(state.name)
            });
        }
        updateStates();
        $rootScope.$on('$stateChangeSuccess', function(event, toState, fromParams, fromState) {
            updateStates();
            $rootScope.before = fromState;
            // console.log($rootScope.states);
        });
        $rootScope.$watch("$viewContentLoaded", function(event) {
            // $rootScope.main.debug="fk";
        })
        $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
            //console.log($state);

        });

    });
