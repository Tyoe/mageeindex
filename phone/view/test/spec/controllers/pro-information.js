'use strict';

describe('Controller: ProInformationCtrl', function () {

  // load the controller's module
  beforeEach(module('mageeViewApp'));

  var ProInformationCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ProInformationCtrl = $controller('ProInformationCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ProInformationCtrl.awesomeThings.length).toBe(3);
  });
});
