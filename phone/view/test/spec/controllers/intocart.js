'use strict';

describe('Controller: IntocartCtrl', function () {

  // load the controller's module
  beforeEach(module('mageeViewApp'));

  var IntocartCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    IntocartCtrl = $controller('IntocartCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(IntocartCtrl.awesomeThings.length).toBe(3);
  });
});
