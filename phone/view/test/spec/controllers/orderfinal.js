'use strict';

describe('Controller: OrderfinalCtrl', function () {

  // load the controller's module
  beforeEach(module('mageeViewApp'));

  var OrderfinalCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    OrderfinalCtrl = $controller('OrderfinalCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(OrderfinalCtrl.awesomeThings.length).toBe(3);
  });
});
