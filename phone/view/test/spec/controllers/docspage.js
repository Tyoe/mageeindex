'use strict';

describe('Controller: DocspageCtrl', function () {

  // load the controller's module
  beforeEach(module('mageeViewApp'));

  var DocspageCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    DocspageCtrl = $controller('DocspageCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(DocspageCtrl.awesomeThings.length).toBe(3);
  });
});
