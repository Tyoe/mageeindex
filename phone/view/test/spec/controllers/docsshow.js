'use strict';

describe('Controller: DocsshowCtrl', function () {

  // load the controller's module
  beforeEach(module('mageeViewApp'));

  var DocsshowCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    DocsshowCtrl = $controller('DocsshowCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(DocsshowCtrl.awesomeThings.length).toBe(3);
  });
});
