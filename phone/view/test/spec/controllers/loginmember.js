'use strict';

describe('Controller: LoginmemberCtrl', function () {

  // load the controller's module
  beforeEach(module('mageeViewApp'));

  var LoginmemberCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    LoginmemberCtrl = $controller('LoginmemberCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(LoginmemberCtrl.awesomeThings.length).toBe(3);
  });
});
