'use strict';

describe('Controller: OtherdocsCtrl', function () {

  // load the controller's module
  beforeEach(module('mageeViewApp'));

  var OtherdocsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    OtherdocsCtrl = $controller('OtherdocsCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(OtherdocsCtrl.awesomeThings.length).toBe(3);
  });
});
