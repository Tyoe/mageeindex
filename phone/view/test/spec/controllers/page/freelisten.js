'use strict';

describe('Controller: PageFreelistenCtrl', function () {

  // load the controller's module
  beforeEach(module('mageeViewApp'));

  var PageFreelistenCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PageFreelistenCtrl = $controller('PageFreelistenCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(PageFreelistenCtrl.awesomeThings.length).toBe(3);
  });
});
