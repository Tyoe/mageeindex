'use strict';

describe('Controller: PageNavcontentCtrl', function () {

  // load the controller's module
  beforeEach(module('mageeViewApp'));

  var PageNavcontentCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PageNavcontentCtrl = $controller('PageNavcontentCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(PageNavcontentCtrl.awesomeThings.length).toBe(3);
  });
});
