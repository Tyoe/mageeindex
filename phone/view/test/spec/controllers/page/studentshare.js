'use strict';

describe('Controller: PageStudentshareCtrl', function () {

  // load the controller's module
  beforeEach(module('mageeViewApp'));

  var PageStudentshareCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PageStudentshareCtrl = $controller('PageStudentshareCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(PageStudentshareCtrl.awesomeThings.length).toBe(3);
  });
});
