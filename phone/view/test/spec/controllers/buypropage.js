'use strict';

describe('Controller: BuypropageCtrl', function () {

  // load the controller's module
  beforeEach(module('mageeViewApp'));

  var BuypropageCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    BuypropageCtrl = $controller('BuypropageCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(BuypropageCtrl.awesomeThings.length).toBe(3);
  });
});
