'use strict';

describe('Controller: ShopchartCtrl', function () {

  // load the controller's module
  beforeEach(module('mageeViewApp'));

  var ShopchartCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ShopchartCtrl = $controller('ShopchartCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ShopchartCtrl.awesomeThings.length).toBe(3);
  });
});
