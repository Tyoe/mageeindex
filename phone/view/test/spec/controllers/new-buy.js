'use strict';

describe('Controller: NewBuyCtrl', function () {

  // load the controller's module
  beforeEach(module('mageeViewApp'));

  var NewBuyCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    NewBuyCtrl = $controller('NewBuyCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(NewBuyCtrl.awesomeThings.length).toBe(3);
  });
});
