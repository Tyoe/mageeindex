'use strict';

describe('Filter: totmoney', function () {

  // load the filter's module
  beforeEach(module('mageeViewApp'));

  // initialize a new instance of the filter before each test
  var totmoney;
  beforeEach(inject(function ($filter) {
    totmoney = $filter('totmoney');
  }));

  it('should return the input prefixed with "totmoney filter:"', function () {
    var text = 'angularjs';
    expect(totmoney(text)).toBe('totmoney filter: ' + text);
  });

});
