'use strict';

describe('Filter: httpctrl', function () {

  // load the filter's module
  beforeEach(module('mageeViewApp'));

  // initialize a new instance of the filter before each test
  var httpctrl;
  beforeEach(inject(function ($filter) {
    httpctrl = $filter('httpctrl');
  }));

  it('should return the input prefixed with "httpctrl filter:"', function () {
    var text = 'angularjs';
    expect(httpctrl(text)).toBe('httpctrl filter: ' + text);
  });

});
