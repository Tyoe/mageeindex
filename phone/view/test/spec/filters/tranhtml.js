'use strict';

describe('Filter: tranhtml', function () {

  // load the filter's module
  beforeEach(module('mageeViewApp'));

  // initialize a new instance of the filter before each test
  var tranhtml;
  beforeEach(inject(function ($filter) {
    tranhtml = $filter('tranhtml');
  }));

  it('should return the input prefixed with "tranhtml filter:"', function () {
    var text = 'angularjs';
    expect(tranhtml(text)).toBe('tranhtml filter: ' + text);
  });

});
