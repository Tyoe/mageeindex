'use strict';

describe('Filter: tranIfa', function () {

  // load the filter's module
  beforeEach(module('mageeViewApp'));

  // initialize a new instance of the filter before each test
  var tranIfa;
  beforeEach(inject(function ($filter) {
    tranIfa = $filter('tranIfa');
  }));

  it('should return the input prefixed with "tranIfa filter:"', function () {
    var text = 'angularjs';
    expect(tranIfa(text)).toBe('tranIfa filter: ' + text);
  });

});
