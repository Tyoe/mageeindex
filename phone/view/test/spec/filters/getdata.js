'use strict';

describe('Filter: getdata', function () {

  // load the filter's module
  beforeEach(module('mageeViewApp'));

  // initialize a new instance of the filter before each test
  var getdata;
  beforeEach(inject(function ($filter) {
    getdata = $filter('getdata');
  }));

  it('should return the input prefixed with "getdata filter:"', function () {
    var text = 'angularjs';
    expect(getdata(text)).toBe('getdata filter: ' + text);
  });

});
