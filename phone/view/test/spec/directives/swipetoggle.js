'use strict';

describe('Directive: swipetoggle', function () {

  // load the directive's module
  beforeEach(module('mageeViewApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<swipetoggle></swipetoggle>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the swipetoggle directive');
  }));
});
