'use strict';

describe('Directive: changeToComputer', function () {

  // load the directive's module
  beforeEach(module('mageeViewApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<change-to-computer></change-to-computer>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the changeToComputer directive');
  }));
});
