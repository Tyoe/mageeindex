<?php

namespace App\Http\Controllers;

class chmail extends Controller {

	public $mail_to;
	public $mail_from;
	public $mail_title;
	public $mail_body;
	public $subscriber_name;
	public $order_id;

	public function chmail() {
		$this->mail_title = "感謝您購買馬跡中心產品課程";
		$this->mail_from = "magee@magee.tw";
		$this->mail_to = "";
		$this->mail_body = "";

		$this->order_id = "";
		$this->subscriber_name = "";
		date_default_timezone_set("Asia/Taipei");
	}

	// 寄送信件
	public function send_mail($mail_to) {

		////////////////////////////////////////////////////
		//
		// 中華電信 mail() 寄信函數
		// 會員認證信
		//
		////////////////////////////////////////////////////

		/* 自行設定參數 */

		// 信件標頭
		$mail_title = $this->mail_title;

		// 收件人信箱
		//$mail_to = $this->mail_to;
		//$mail_to = "paulsets@yahoo.com";
		$mail_body = $this->mail_body;

		/* 設定參數結束*/

		$mail_from = $this->mail_from; //填入測試寄件人郵件
		$mail_subject = "=?UTF-8?B?" . base64_encode($mail_title) . " ?=";
		$mail_header_str = "Content-Type:text/html;charset=UTF-8\r\nFrom:$mail_from\r\nreply-to:$mail_from\r\n";

		return $return_arg = mail($mail_to, $mail_subject, $mail_body, $mail_header_str);

	}

	public function build_order_mail($ord_no, $ord_date, $ord_name, $order_cnt) {
		// $database = "vhost86454";
		// $dbserver = "127.0.0.1";
		// $dbuser = "vhost86454";
		// $dbpasswd = "27710778";
		// $link = mysql_connect($dbserver, $dbuser, $dbpasswd) or die('connect erro！');
		// mysql_query("SET NAMES 'utf8'");
		// mysql_query("SET CHARACTER_SET_CLIENT=utf8");
		// mysql_query("SET CHARACTER_SET_RESULTS=utf8");
		// $sql = "select*from ORDERS where `uid`='{$uid}'";
		// $ty = mysql_query($sql);
		// $order_data = mysql_fetch_assoc($ty);
		// $order_cnt = str_replace("<br />", ",", $order_data['order_cnt_name']);
		$data = "";
		$data .= '
<html><head><title>馬跡庫比</title></head>
<body>
<table style="width:640px;font-size:12px;border:1px solid #aaa;padding:5px;background-color:#292929 ">
	<tr>
    	<td colspan="2" style="width:650px; background-color:#eee;color:#333333;">
		<img src="http://www.magee.tw/mail/banner.jpg" />
        </td>
    </tr>

	<tr>
    	<td style="width:70px; padding:10px; background-color:#ffe7c4">
        	<table width="100%"><tr><td style="background-color:#ff9800;color:#ffffff;padding:10px;">訂購說明</td></tr></table>
        </td>

        <td style="width:515px; padding:10px; background-color:#ffe7c4;color:#000000;">
親愛的 ' . $ord_name . ' 學員，感謝您購買本中心產品 / 課程(' . $order_cnt .

			')<br /><br />
為加速您取得購買之產品，請選擇以下之繳款方式，並於完款後與我們聯繫確認您的資料及座位，完成您的購買手續。
服務專線：02-2733-8118 (上班時段/週一至週五10:00 - 18:00 歡迎來電諮詢)。</td>
        <td></td>
    </tr>

	<tr>
    	<td style="width:70px; padding:10px; background-color:#ffe7c4">
        	<table width="100%"><tr><td style="background-color:#ff9800;color:#ffffff;padding:10px;">繳款方式</td></tr></table>
        </td>

        <td style="width:515px; padding:10px; background-color:#ffe7c4;color:#000000;">
1. 銀行匯款:	&nbsp;&nbsp;帳號：4801-027-027-88<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
臺北富邦銀行 和平分行
<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
戶名：馬跡庫比有限公司<br /><br />
2. ATM轉帳:&nbsp;&nbsp;帳號：4801-027-027-88
銀行代號：012<br /><br />
3. 現場繳款:	 &nbsp;&nbsp;請至馬跡中心(ADD：台北市大安區復興南路二段268號3樓之一)
</td>
        <td></td>
    </tr>
	<tr>
    	<td style="width:70px; padding:10px; background-color:#f8ded4">
        	<table width="100%"><tr><td style="background-color:#ec855c;color:#ffffff;padding:10px;">訂單編號</td></tr></table>
        </td>
        <td style="width:515px; padding:10px; background-color:#f8ded4;color:#000;">
        	<b>' . $ord_no . '</b> (登入馬跡庫比網站帳號後，即可點選<a href="http://www.magee.tw" target="_new">【訂單查詢】</a>查詢您的訂單資料)
        </td>
    </tr>
	<tr>
    	<td style="width:70px; padding:10px; background-color:#f8ded4">
        	<table width="100%"><tr><td style="background-color:#ec855c;color:#ffffff;padding:10px;">訂購日期</td></tr></table>
        </td>
        <td style="width:515px; padding:10px; background-color:#f8ded4;color:#000;"><b>' . $ord_date . '</b></td>
        <td></td>
    </tr>
    <tr>
    	<td style="width:70px; padding:10px; background-color:#f8f8f8">
        	<table width="100%"><tr><td style="background-color:#b0b0b0;color:#ffffff;padding:10px;">會員優惠</td></tr></table>
        </td>
    	<td  style="width:515px; padding:10px; background-color:#f8f8f8;color:#000;">
凡加入馬跡中心之會員可享─
<br />
<br />【線上測驗】線上考題測驗平台，現在報名再送點數
<br />【課程優惠】凡購買課程或商品者，加購考前衝刺班，即享折扣！
<br />【重要公告】報名日期公告、考試重要公告
<br />【法規更新】最新考試法規修正通知
<br />【內容更新】最新考試內容修正通知
<br />【學員福利】實習機會、工作職缺、相關課程
        </td>
    </tr>
</table>
</body></html>
';
		$this->mail_body = $data;
	}
	public function joinmail($member_cname, $pk, $member_id) {
		$mail_title = "馬跡中心會員帳號確認函件";
		// 會員姓名
		$mail_member_cname = $member_cname;
		// 認證主鍵
		$mail_member_pk = $pk;
		// 收件人信箱
		$mail_to = $member_id;
		$this->mail_body = "
		<html>
		<head>
			<title>{$mail_title}</title>
		</head>
		<body>
		<table width=\"520\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
		<tr>
		<td height=\"25\" align=\"left\">" . $mail_member_cname . " 先生/小姐 您好!!&nbsp;&nbsp;&nbsp;歡迎您加入馬跡中心會員</td>
		</tr>
		<tr>
		<td height=\"25\" align=\"left\">為了確保您的帳號安全</td>
		</tr>
		<tr>
		<td height=\"25\" align=\"left\">請點選 <a href=\"http://www.magee.tw/_cfm.php?linkno={$mail_member_pk}\">[帳號確認連結]</a> 完成帳號確認動作!!!</td>
		</tr>
		<tr>
		<td height=\"25\" align=\"left\">您的帳號才會正式啟用</td>
		</tr>
		<tr>
		<td height=\"25\" align=\"left\">馬跡領隊導遊訓練‧派遣中心   www.magee.tw</td>
		</tr>
		<tr>
		<td height=\"25\" align=\"left\">諮詢專線：02-2733-8118 / 02-7711-0050 </td>
		</tr>
		<tr>
		<td height=\"25\" align=\"left\">地址：106 台北市大安區復興南路二段268號5樓</td>
		</tr>
		</table>
		</body>
		</html>
		";
		return $this->mail_body;
	}
	public function memberjoin($member_cname, $pk, $member_id) {
		return "sd";
// 		$mail_title = "馬跡中心會員帳號確認函件";
		// 		return $mail_title;
		// // 會員姓名
		// 		$mail_member_cname = $member_cname;
		// // 認證主鍵
		// 		$mail_member_pk = $pk;
		// // 收件人信箱
		// 		$mail_to = $member_id;
		// //GUSR
		// 		$this->mail_body = "
		// <html>
		// <head>
		// 	<title>{$mail_title}</title>
		// </head>
		// <body>
		// <table width=\"520\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
		// <tr>
		// <td height=\"25\" align=\"left\">" . $mail_member_cname . " 先生/小姐 您好!!&nbsp;&nbsp;&nbsp;歡迎您加入馬跡中心會員</td>
		// </tr>
		// <tr>
		// <td height=\"25\" align=\"left\">為了確保您的帳號安全</td>
		// </tr>
		// <tr>
		// <td height=\"25\" align=\"left\">請點選 <a href=\"http://www.magee.tw/_cfm.php?linkno={$mail_member_pk}\">[帳號確認連結]</a> 完成帳號確認動作!!!</td>
		// </tr>
		// <tr>
		// <td height=\"25\" align=\"left\">您的帳號才會正式啟用</td>
		// </tr>
		// <tr>
		// <td height=\"25\" align=\"left\">馬跡領隊導遊訓練‧派遣中心   www.magee.tw</td>
		// </tr>
		// <tr>
		// <td height=\"25\" align=\"left\">諮詢專線：02-2733-8118 / 02-7711-0050 </td>
		// </tr>
		// <tr>
		// <td height=\"25\" align=\"left\">地址：106 台北市大安區復興南路二段268號5樓</td>
		// </tr>
		// </table>
		// </body>
		// </html>
		// ";
	}
}
