<?php

namespace App\Http\Controllers;
use DB;
trait SubController {
	//
	private function countseadmoneya($ki) {
		$books = $ki; // Cart list books_id
		$keys = $books->keys();

		$kk = array();
		$x = $y = $z = 0;
		$de = 0; //餘數
		$small = 0; //31*23.3*7.3
		$mid = 0; //31*22.8*10.3
		$big = 0; //39.5*27.5*23
		$buysum = [];
		$bag = array(45, 10.3, 3, 0);
		$money = array(110, 80, 65);
		$b = array(0, 0, 0);
		$totley = 0;
		$tmptt = 0;
		$koo = true;
		$book = DB::table("BOOKS")->whereIn("books_id", $keys)->orderBy("booky", "desc")->get(["books_id", "bookx", "bookz", "booky", "shipment"]);
		$itema = $book->map(function ($tmp, $keys) use ($books, $totley) {
			$totley = $tmp->booky * $books[$tmp->books_id];
			return ["number" => $books[$tmp->books_id], "book_id" => $tmp->books_id, "booky" => $tmp->booky, "shipment" => $tmp->shipment, "totley" => $totley];
		});
		$totley = $itema->sum(["totley"]);
		$buysum = $itema->map(function ($item, $keys) {
			$item = collect($item)->forget("totley");
			return $item->all();
		})->toArray();

		$tmptt = $buysum[0]['shipment'];
		if (count($buysum) == 1) {
			if ($buysum[0]['number'] == '1' && $buysum[0]['shipment'] == '60') {
				return ["deliery" => 60, "box" => $buysum];
				$koo = false;
			}
		}

		if ($koo) {
			$tmpa = 0;
			$tr = true;
			$tot = 0;
			$zj = [];
			$kka = [];
			$try = 0;
			$kal = [];
			for ($kk = 1; $kk < count($bag); $kk++) {
				for ($i = 0; $i < count($buysum); $i++) {
					$number = $buysum[$i]['number'];
					for ($k = 1; $k <= $number; $k++) {
						$tot += $buysum[$i]['booky'];
						array_push($zj, $i);
						if ($bag[$kk] < $tot) {
							if ($tr) {
								$b[$kk - 1]++;
								$tr = false;
							}
							if ($bag[$kk - 1] < $tot) {
								$tp = $bag[$kk - 1] - ($tot - $buysum[$i]['booky']);
								array_pop($zj);
								$tr = true;
								$tlo = count($buysum);
								$tot = 0;
								for ($uu = $i + 1; $uu < $tlo; $uu++) {
									if ($tp > $buysum[$uu]['booky']) {
										$tto = $buysum[$uu]['number'];
										for ($kko = 0; $kko < $ttol; $kko++) {
											if ($buysum[$uu]['booky'] < $tp) {
												$tp = $tp - $buysum[$uu]['booky'];
												array_push($zj, $uu);
											} else {
												continue;
											}
										}
									} else {
										$tp = $tp;
									}
								}
								foreach ($zj as $o) {
									$buysum[$o]['number']--;
								}
								$zj = array();
							}
						}
					}
				}
				if (!($tr) && $bag[$kk - 1] > $tot) {
					$tr = true;
					foreach ($zj as $o) {
						$buysum[$o]['number']--;
					}
					$zj = array();

				}
				$tot = 0;
			}
			$totel = 0;
			for ($i = 0; $i < count($b); $i++) {
				$totel += $b[$i] * $money[$i];
			}
			return ["deliery" => $totel, "box" => $b];
		}
	}
	private function fn_column_array($tbl, $column, $where) {
		$q1 = "select $column from $tbl where $where";
		$r1 = DB::select($q1);
		$array = $r1[0];
		return $array;
	}
	private function fn_column_value($tbl, $column, $where) {
		$q1 = "select $column from $tbl where $where";
		$r1 = DB::select($q1);
		$content = $r1[0];
		return $content;
	}
	private function fn_id_coded($OP_TABLE) {
		$_OP = $this->fn_column_array("FUNC", "*", "func_id='$OP_TABLE'");
		$ak = "MAX(pk)";
		$tbl_id_pk = $this->fn_column_value("{$OP_TABLE}", "MAX(pk)", "pk>0");
		$tbl_id_pk = $tbl_id_pk->$ak;
		$tbl_id_pk += 1;
		$_OP = collect($_OP);
		if ($_OP['id_type_id'] == "2") {
			$tbl_id_value = $_OP['first_no'];

			$date_rule = "";
			$date_rule_str = "";
			$sql_date = "";

			if ($_OP['section1'] == "YYYY") {
				$date_rule = "Y";
				$date_rule_str .= date('Y');
				$sql_date .= date('Y');
			}
			if ($_OP['section1'] == "YY") {
				$date_rule = "Y";
				$date_rule_str .= date('y');
				$sql_date .= date('Y');
			}

			if ($_OP['section2'] == "MM") {
				$date_rule .= "m";
				$date_rule_str .= date('m');
				$sql_date .= "-" . date('m');
			}

			if ($_OP['section3'] == "DD") {
				$date_rule .= "d";
				$date_rule_str .= date('d');
				$sql_date .= "-" . date('d');
			}

			$tbl_id_value .= $date_rule_str;

			$sql = "";
			if ($date_rule == "Y") {
				$sql = "left(crea_date,4)='$sql_date'";
			}
			if ($date_rule == "Ym") {
				$sql = "left(crea_date,7)='$sql_date'";
			}
			if ($date_rule == "Ymd") {
				$sql = "left(crea_date,10)='$sql_date'";
			}

			$tbl_id_num = $this->fn_column_value("$OP_TABLE", "count(*) as `lk`", $sql);
			$tbl_id_num = $tbl_id_num->lk;
			$tbl_id_num += 1;

			if (!empty($_OP['id_bytes'])) {
				$tbl_id_value .= str_pad($tbl_id_num, $_OP['id_bytes'], "0", STR_PAD_LEFT);
			} else {
				$tbl_id_value .= $tbl_id_num;
			}
		} else {
			$tbl_id_value = $_OP['first_no'];

			if (!empty($_OP['id_bytes'])) {
				$tbl_id_value .= str_pad($tbl_id_pk, $_OP['id_bytes'], "0", STR_PAD_LEFT);
			} else {
				$tbl_id_value .= $tbl_id_pk;
			}
		}

		return $tbl_id_value;
	}
}
