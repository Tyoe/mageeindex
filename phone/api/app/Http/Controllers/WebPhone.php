<?php

namespace App\Http\Controllers;

use App\Http\Controllers\chmail;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class WebPhone extends Controller {
	use SubController;

	public function test(Request $request) {
		$newstext = DB::table("NEW_WEB_NEWS")->where("del", "")->get();
		return $newstext;
	}
	public function getnewtext(Request $request) {
		$mytime = Carbon::now('Asia/Taipei');
		$newstext = DB::table("DOCS")->where(["docs_dpt_id" => "D001", "data_sts" => "Y", "online" => "Y"])->where("offline_date", ">", $mytime)->orderBy("brows_order", "asc")->take(5)->get(['pk', 'docs_cname', "docs_sdpt_id", "docs_dpt_id"]);
		$newstext = collect($newstext)->map(function ($item, $keys) {
			$vk = DB::table("DOCS_SDPT")->where(['docs_sdpt_id' => $item->docs_sdpt_id])->first(['docs_sdpt_cname']);
			$item->ClassName = ($vk) ? $vk->docs_sdpt_cname : "最新消息";
			return $item;
		});
		return $newstext;
	}
	public function getDocstext(Request $request) {
		$Class = $request['Class'];
		$id = $request['id'];
		$kl = DB::table("DOCS")->where(['data_sts' => "Y", "pk" => $id])->first();
		$ClassName = DB::table("DOCS_DPT")->where(['docs_dpt_id' => $kl->docs_dpt_id])->first(['docs_dpt_cname']);
		$SClassName = DB::table("DOCS_SDPT")->where(['docs_sdpt_id' => $kl->docs_sdpt_id])->first(['docs_sdpt_cname']);
		$kl = collect($kl);
		$kl->put("sclassname", ($SClassName) ? $SClassName->docs_sdpt_cname : "最新消息");
		$kl->put("classname", $ClassName->docs_dpt_cname);
		return $kl->all();
	}
	public function getDocsList(Request $request) {
		$mytime = Carbon::now('Asia/Taipei');
		$Class = $request['Class'];
		$dptlist = [];
		$resolut = DB::table("DOCS")->where(["data_sts" => "Y", "online" => "Y"])->where("offline_date", ">", $mytime)->get(['pk', 'docs_cname', "docs_sdpt_id", "docs_dpt_id"]);
		$resolut = collect($resolut)->map(function ($item, $keys) {
			$vk = DB::table("DOCS_SDPT")->where(['docs_sdpt_id' => $item->docs_sdpt_id])->first(['docs_sdpt_cname']);
			$item->ClassName = ($vk) ? $vk->docs_sdpt_cname : "最新消息";
			return $item;
		});
		$dptlist = DB::table("DOCS_DPT")->where("data_sts", "Y")->orderBy("brows_order", "asc")->get(["docs_dpt_id", 'docs_dpt_cname']);
		$dptlist = $dptlist->map(function ($item) {
			$item->ck = true;
			return $item;
		});

		return [$resolut, $dptlist];
	}
	public function getproClass(Request $request) {
		$mytime = Carbon::now('Asia/Taipei');
		$protopinfo = DB::table("WEB_HEAD_INTRO")->where(["uid" => "121208124304"])->first();
		$proClass = collect();
		$overview = DB::table("OVERVIEW")->where(["online" => "Y", "data_sts" => "Y"])->where("offline_date", ">=", $mytime)->where("online_date", "<=", $mytime)->orderBy("brows_order", "asc")->get(['overview_id', 'title', 's_img', 'info_description', 'info_date', 'info_time']);
		$web_head = DB::table("WEB_HEAD_INTRO")->where(["web_head_intro_id" => "course_scheme"])->first(['content']);
		$overview = $overview->map(function ($item, $keys) use ($mytime) {
			$pc = DB::table("COURS")->where(['data_sts' => "Y", "online" => "Y", "overview_id" => $item->overview_id])->where("offline_date", ">", $mytime)->get(['descrip', 'scheme_id', 'cours_cname', 'cours_id', 'pk', 'price', 'promo_note', "from_date", "to_date", "time_str", "online", "qty", "order_qty"])->groupBy("scheme_id")->all();
			$item->info = $pc;
			return $item;
		});
		$scheme = DB::table("SCHEME_DPT")->where(["data_sts" => "Y"])->get(['scheme_dpt_id', "scheme_dpt_cname"]);
		$scheme = $scheme->map(function ($item, $keys) {
			return [$item->scheme_dpt_id => $item->scheme_dpt_cname];
		})->collapse();
		return ["overview" => $overview, "scheme_name" => $scheme, "web_head" => str_replace("&quot;", "\"", $web_head->content)];
	}
	public function getproBook(Request $request) {
		$mytime = Carbon::now('Asia/Taipei');
		$book = DB::table("BOOKS")->where(["data_sts" => "Y", "online" => "Y"])->where("offline_date", ">", $mytime)->where("online_date", "<=", $mytime)->orderBy("brows_order", "asc")->get();
		$book = $book->map(function ($item, $keys) {
			$item->descrip;
			return $item;
		});
		return $book;
	}
	public function getshopcart(Request $request) {
		$mytime = Carbon::now('Asia/Taipei');
		$Order_Log = DB::table("ORDERS_LOG")->where(["data_sts" => "Y", "online" => ""])->where("shopingno", "like", $request->ip() . "%")->get();
		$Order_Log->map(function ($item, $keys) use ($mytime) {
			$ta = DB::table($item->func_id)->where(["data_sts" => "Y", "online" => "Y"])->where("offline_date", ">", $mytime)->where("online_date", "<=", $mytime);
			if ($item->func_id == "BOOKS") {
				$ta->where("onsellout", "Y");
			}
			$ik = $ta->count();
			if ($ik > 0) {
				return $item;
			} else {
				DB::table("ORDERS_LOG")->where("uid", $item->uid)->update(["onilne" => "Y"]);
				return "";
			}
		})->filter(function ($item, $key) {
			return ($item != "");
		});
		return $Order_Log;
	}
	public function setOrder_log(Request $request) {
		$type = $request['type'];
		$id = $request['id'];
		$ip = $request->ip() . date("YmdHis");
		$strlong = strtolower($type);
		$order_log_count = DB::table("ORDERS_LOG")->where(["id_pk" => $id, "data_sts" => "Y", "func_id" => $type, "online" => ""])->where("shopingno", "LIKE", $request->ip() . "%")->count();
		if ($order_log_count == 0) {
			$S_id = $strlong . "_id";
			$Cname = $strlong . "_cname";
			$ak = [$S_id, $Cname, "price"];
			if ($type == 'BOOKS') {
				array_push($ak, "shipment");
			}
			$prodout = DB::table($type)->where("data_sts", "Y")->where("pk", $id)->first($ak);
			$insertfild = ["data_sts" => "Y", "online" => "", "shopingno" => $ip, "func_id" => $type, "id" => $prodout->{$S_id}, "id_cname" => $prodout->{$Cname}, "qty" => "1", "price" => $prodout->price, "id_pk" => $id, "delivery_m" => (isset($prodout->shipment) ? $prodout->shipment : "0"), "delivery" => "0"];
			DB::table("ORDERS_LOG")->insert([$insertfild]);
		}

		return "true";
	}
	public function countseadmoney(Request $request) {
		$order_log_count = DB::table("ORDERS_LOG")->where(["data_sts" => "Y", "func_id" => "BOOKS", "online" => "", "func_id" => "BOOKS"])->where("shopingno", "LIKE", $request->ip() . "%")->get();
		$book = collect();
		$delivery = ["deliery" => 0, "box" => []];
		if ($order_log_count->count() > 0) {
			$order_log_count->each(function ($item, $keys) use ($book) {
				$book[$item->id] = $item->qty;
			});
			$delivery = $this->countseadmoneya($book);
		}
		return $delivery;
	}

	public function changeqty(Request $request) {
		$data = $request['data'];
		DB::table("ORDERS_LOG")->where("pk", $data['pk'])->update(["qty" => $data['qty']]);
		return $data;

	}
	public function rmitem(Request $request) {
		$data = $request['data'];
		$id = $data['pk'];
		DB::table("ORDERS_LOG")->where("pk", $id)->update(["data_sts" => "D"]);
		return $this->getshopcart($request);
	}
	public function changedelivery(Request $request) {
		$order_log_count = DB::table("ORDERS_LOG")->where(["data_sts" => "Y", "func_id" => "BOOKS"])->where("shopingno", "LIKE", "%" . $request->ip() . "%")->update(["delivery" => $request['data']]);
		return $request->all();
	}
	public function getpost_id() {
//select post_id,post_city,post_area from POST order by post_order,post_id
		$post = DB::table("POST")->where("data_sts", "Y")->orderBy("post_id", "asc")->get(["post_id", "post_city", "post_area"]);
		return $post;
	}
	public function setOrder(Request $request) {
		$mytime = Carbon::now('Asia/Taipei');
		// $data = json_decode('{"buylist":[{"pk":17507},{"pk":17506},{"pk":17505}],"ordermember":{"name":"周信榮","phone_no":"0921467841","mobil_no":"0921467841","email":"james60713@gmail.com","post_id":"105","address":"呵呵","notes":"無"},"delivermember":{"name":"周信榮","phone_no":"0921467841","mobil_no":"0921467841","post_id":"105","address":"呵呵","notes":"無"},"delivermoney":"80"}', true);
		$data = $request;
		$ordermember = $data['ordermember'];
		$delivermember = $data['delivermember'];
		$delivermoney = $data['delivermoney'];
		try {
			DB::beginTransaction();
			$orders_id = $this->fn_id_coded("ORDERS");
			$uid = $orders_id . date('YmdHis');
			$insertfield = collect([
				"orders_id" => $orders_id,
				"do_date" => $mytime->toDateString(),
				"member_id" => (isset($data['memberuid'])) ? $data['memberuid'] : "",
				"name" => $ordermember['name'],
				"phone_no" => $ordermember['phone_no'],
				"mobil_no" => $ordermember['mobil_no'],
				"email" => $ordermember['email'],
				"post_id" => $ordermember['post_id'],
				"address" => $ordermember['address'],
				"inv_no" => (isset($delivermember['inv_no'])) ? $delivermember['inv_no'] : "",
				"inv_head" => (isset($delivermember['inv_head'])) ? $delivermember['inv_head'] : "",
				"order_name" => $delivermember['name'],
				"order_phone_no" => $delivermember['phone_no'],
				"order_mobil_no" => $delivermember['mobil_no'],
				"order_post_id" => $delivermember['post_id'],
				"order_address" => $delivermember['address'],
				"notes" => (isset($delivermember['notes'])) ? $delivermember['notes'] : "",
				"order_receipt" => "undone_receipt",
				"uid" => $uid,
				"crea_date" => $mytime]);
			$q = "select * from ORDERS_LOG where shopingno like '%" . $request->ip() . "%' and data_sts='Y' and online='' order by pk";

			$r = DB::table("ORDERS_LOG")->where(["data_sts" => "Y", "online" => ""])->where("shopingno", "like", $request->ip() . "%")->get();

			$n = $r->count();
			// return $n;
			if ($n > 0) {
				$o_amt = 0;
				$o_cnt_name = "";
				$amz = 0;
				$v = 0;
				$w = 0;
				$x = 0;
				$y = 0;
				$z = 0;

				$tmpa = $r->map(function ($ra, $keys) use ($o_amt, $insertfield) {
					$amt = $ra->qty * $ra->price;
					$sql2 = $insertfield->map(function ($item) {return $item;});
					$o_cnt_name = "({$ra->id})" . $ra->id_cname;
					$sql2 = $sql2->merge([
						"func_id" => $ra->func_id,
						"id_pk" => $ra->id_pk,
						"id" => $ra->id,
						"id_cname" => $ra->id_cname,
						"qty" => $ra->qty,
						"price" => $ra->price,
						"amt" => $amt,
						"del_type_id" => ""]);
					// dd($sql2);
					DB::table("ORDERS_CNT")->insert([$sql2->all()]);
					return ["price" => $amt, "comment" => $o_cnt_name];
				});

				$o_amt = $tmpa->sum("price");
				$o_cnt_name = $tmpa->implode("comment", "<br />");

				$delz = $delivermoney;
				$orders_notes = "運費:" . $delz;
				$insertfield = $insertfield->merge(["o_amt" => ($o_amt + $delz), "order_sts" => "未處理", "order_cnt_name" => $o_cnt_name, "order_notes" => $orders_notes]);

				DB::table("ORDERS")->insert([$insertfield->all()]);
				DB::table("ORDERS_CNT")->where("uid", $uid)->update(["o_amt" => $o_amt, "order_sts" => '未處理', "deliver_sts" => '未處理']);
				DB::table("ORDERS_LOG")->where(["data_sts" => "Y", "online" => ""])->where("shopingno", "like", $request->ip() . "%")->update(["online" => "Y", "upd_date" => $mytime]);
			}
			DB::commit();
			$ordermember = $data['ordermember'];
			$delivermember = $data['delivermember'];
			$order_cnt = DB::table("ORDERS_CNT")->where("uid", $uid)->get(["func_id", "id_pk", "id", "id_cname", "qty", "price"]);
			$post = $this->getpost_id();
			$email_pro = $order_cnt->implode("id_cname", ",");
			$mail = new chmail();
			$mail->chmail();
			$mail->build_order_mail($orders_id, date('M,d Y'), $ordermember['name'], $email_pro);
			$mail->send_mail(trim($ordermember['email']));
			$mail->send_mail("mageecube@hotmail.com");
			return ["state" => "success", "uid" => $uid, "delivermoney" => $delivermoney, "ordermember" => $ordermember, "delivermember" => $delivermember, "order_cnt" => $order_cnt, "post_id" => $post];
		} catch (\Exception $e) {
			DB::rollback();
			return ["state" => "error"];
		};
	}
	public function login(Request $request) {
		$kl = ['online' => "Y", "member_id" => $request->account, "password" => $request->password, "data_sts" => "Y"];
		$member = DB::table("MEMBER")->where($kl)->get()->map(function ($item) {
			$item->email = ($item->email) ? $item->email : $item->member_id;
			$item->phone_no = ($item->phone_no) ? $item->phone_no : $item->mobil_no;
			return $item;
		});
		return $member;
	}
	public function getNavcontent(Request $request) {
		$oi = DB::table("TOPICON")->where(["data_sts" => "Y", "pk" => 5])->first(['content']);
		return $oi->content;
	}
	public function gethotnews(Request $request) {
		$hotnews = DB::table("NEW_WEB_NEWS")->where(["del" => "", "show_wi" => "1"])->get(["name", "url"]);
		return $hotnews;
	}
	public function tees() {
		$mail = new chmail();
		$mail->chmail();
		//$mail->mail_body = "hello";
		$mail->build_order_mail($orders_id, date('M,d Y'), $_POST[name], $uid);
		// $mail::send_mail("james60713@gmail.com");
		// return $mail->mail_title;
		//$ti = $mail->send_mail("james60713@gmail.com");
		return ["State" => $mail->mail_body];
	}
	public function registered(Request $request) {
		$mytime = Carbon::now('Asia/Taipei');
		$memberdata = $request['member'];
		$kl = collect($memberdata);
		$member = DB::table("MEMBER")->where("member_id", $kl['member_id'])->count();
		$resolut = [];
		if ($member > 0) {
			$resolut = ['state' => "double"];
		} else {
			$orders_id = $this->fn_id_coded("MEMBER");
			$uid = $orders_id . date('YmdHis');
			$kl->put("uid", $uid);
			$kl->put("data_sts", "Y");
			$kl->put("point", "200");
			$kl->put("crea_date", $mytime);

			$id = DB::table("MEMBER")->insertGetId($kl->all());
			$mail = new chmail();
			$mail->chmail();
			$mail->joinmail($kl['member_cname'], $id, $kl['member_id']);
			$mail->send_mail($kl['member_id']);
			$mail->send_mail("0932384168@yahoo.com.tw");
			$resolut = ['state' => "newjoin", "email" => $kl['member_id']];
		}
		return $resolut;
	}
	public function get_information(Request $request) {

		$k = DB::table($request['func_id'])->where("pk", $request['id'])->get();
		return $k;
	}
}
