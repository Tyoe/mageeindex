<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::get("/test/test", "WebPhone@test");
Route::get("/getnewtext", "WebPhone@getnewtext");
Route::get("/getDocstext", "WebPhone@getDocstext");
Route::get("/getDocsList", "WebPhone@getDocsList");
Route::get("/getproClass", "WebPhone@getproClass");
Route::get("/getproBook", "WebPhone@getproBook");
Route::get("/getshopcart", "WebPhone@getshopcart");
Route::get("/setOrder_log", "WebPhone@setOrder_log");
Route::get("/countseadmoney", "WebPhone@countseadmoney");
Route::post("/changeqty", "WebPhone@changeqty");
Route::post("/rmitem", "WebPhone@rmitem");
Route::get("/changedelivery", "WebPhone@changedelivery");
Route::get("/getpost_id", "WebPhone@getpost_id");
Route::post("/setOrder", "WebPhone@setOrder");
Route::post("/login", "WebPhone@login");
Route::get("getNavcontent", "WebPhone@getNavcontent");
Route::get("gethotnews", "WebPhone@gethotnews");
Route::post("registered", "WebPhone@registered");
Route::get("get_information", "WebPhone@get_information");
