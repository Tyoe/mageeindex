'use strict';

/**
 * @ngdoc overview
 * @name onlinetestApp
 * @description
 * # onlinetestApp
 *
 * Main module of the application.
 */
angular
    .module('onlinetestApp', [
        'ngAnimate',
        'ngCookies',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ngTouch',
        'ui.router',
        'satellizer',
        'ui.bootstrap',
        "ngLoadingSpinner",
        "chart.js",
        'ui.tinymce',
        'ngDraggable',
    ])
    .config(function($stateProvider, $urlRouterProvider, $authProvider, $httpProvider) {
        $urlRouterProvider.otherwise('/');
        $stateProvider.state("index", {
            url: '/',
            controller: 'MainCtrl',
            controllerAs: 'main',
            templateUrl: 'views/main.html'
        })
    });
