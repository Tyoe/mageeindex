'use strict';

/**
 * @ngdoc function
 * @name onlinetestApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the onlinetestApp
 */
angular.module('onlinetestApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
