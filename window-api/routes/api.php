<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */
Route::post("Login", "Login@Login");
Route::post("joinmagee", "joinmagee@Login");
Route::get("getClassPageList", "Product@ClassPage");
Route::get("getBookPageList", "Product@BookPage");
Route::get("getpromore", "Product@getpromore");
Route::post("getCartPro", "Product@getCartPro");
Route::get("getpost", "Login@getpost");
Route::post("changedelvary", "Product@changedelvary");
Route::post("submitorder", "Product@submitorder");
Route::get("getDocsGroupList", "Page@getDocsGroupList");
Route::get("getDocList", "Page@getDocList");
Route::get("getDocsInfor", "Page@getDocsInfor");
Route::get("getIndex", "Page@getIndex");
Route::get("getAd", "Page@getAd");
Route::get("getjoindata", "Login@getjoindata");
Route::post("joinmagee", "Login@joinmagee");
Route::group(['middleware' => ['auth:api']], function () {
	Route::get("getmember", "Login@checkedmember");
	Route::post("editpwd", "Memberinfo@editpwd");
	Route::get("getmemberinfo", "Memberinfo@getmemberinfo");
});
