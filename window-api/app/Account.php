<?php

namespace App;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Account extends Authenticatable {
	protected $table = "member";
	protected $fillable = [];
	public $timestamps = false;
}
