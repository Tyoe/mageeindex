<?php

namespace App\Http\Controllers;

use Auth;
use Carbon\Carbon;
use DB;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class Product extends Controller {
	public function ClassPage(Request $request) {
		$today = Carbon::now('Asia/Taipei');
		$Sitting = DB::table("WebSitting")->where(["nydel" => "", "public_class" => "class"])->orderBy("orders", "desc")->first(["name", "context", "remark"]);
		$ProClass = DB::table("outClass as a")->whereExists(function ($query) {
			$query->select(DB::raw(1))->from("product_class as b")->whereRaw("b.nydel='' and b.nyclass='1' and b.uid=a.class");
		})->get(["id", "uid", "name", "color"]);

		$ProClass = $ProClass->map(function ($item, $keys) use ($today) {
			$Classpro = DB::table("WebClassPro")->where(["nydel" => "", "Class" => $item->uid])->get(["bimage", "context", "newtext", "prolist", "uid", "id", "name"])->map(function ($item) use ($today) {
				$prolist = explode(",", $item->prolist);
				// dd(DB::table("product")->where(["nydel" => "", "isonline" => "1"])->get());
				$item->product = $this->productlist($prolist);
				return $item;
			})->filter(function ($item) {return $item->product->count() > 0;})->values();
			$item->WebClassPro = $Classpro;
			return $item;
		})->filter(function ($item) {
			return $item->WebClassPro->count() > 0;
		});
		return ["page" => $Sitting, "ProClass" => $ProClass];
	}
	public function BookPage(Request $request) {
		$today = Carbon::now('Asia/Taipei');
		// $Sitting = DB::table("WebSitting")->where(["nydel" => "", "public_class" => "class"])->orderBy("orders", "desc")->first(["name", "context", "remark"]);
		$ProClass = DB::table("outClass as a")->whereExists(function ($query) {
			$query->select(DB::raw(1))->from("product_class as b")->whereRaw("b.nydel='' and b.nyentity='1' and b.uid=a.class");
		})->get(["id", "uid", "name", "color"]);

		$ProClass = $ProClass->map(function ($item, $keys) use ($today) {

			$Product = DB::table("product")->where(["nydel" => "", "outclass" => $item->uid])->get(["uid"])->implode("uid", ",");
			$pro = $this->productlist(explode(",", $Product));

			$item->product = $pro;
			return $item;
		})->filter(function ($item) {
			return count($item->product) > 0;
		});
		return ["ProClass" => $ProClass];
	}
	private function productlist($prolist) {
		$today = Carbon::now('Asia/Taipei');
		return DB::table("product")->where(["nydel" => "", "isonline" => "1"])->where(function ($query) use ($today) {
			$query->where("ontime", "<=", $today)->where("outtime", ">", $today);
		})->whereIn("uid", $prolist)->get(["id", "uid", "name", "prolistcontext", "image", "outcode"])->map(function ($item) use ($today) {
			$price = DB::table("addprice")->where(["nydel" => "", "belong_pro" => $item->uid, "isonline" => "1"])->first();
			if ($price) {
				$item->price = $price->price;
				$item->pricekid = $price->uid;
			} else {
				$price = DB::table("addprice")->where(["nydel" => "", "belong_pro" => $item->uid])->where(function ($query) use ($today) {
					// $query->where("starttime", "=<", $today)->where("endtime", ">", $today);
				})->orderBy("orders", "asc")->first();
				$item->price = ($price) ? $price->price : NULL;
				$item->pricekid = ($price) ? $price->uid : NULL;
			}
			$item->uidcode = $item->outcode;
			return $item;
		})->filter(function ($item) {return !!$item->price;});
	}
	private function prolist($product) {
		$prolist = explode(",", $product);
		$pro_ele = DB::table("pro_ele");
	}
	public function getpromore(Request $request) {
		$case = $request["caseid"];
		$proid = $request["classid"];
		$result = collect();
		if ($case > 0) {
			$prolista = DB::table("WebClassPro")->where(["nydel" => "", "id" => $case])->first(["prolist", "name"]);
			$prolist = ($prolista) ? explode(",", $prolista->prolist) : NULL;
			if ($prolist) {
				$pro = $this->productlist($prolist);
				$result->put("ProCase", $pro);
				$result->put("ProName", $prolista);
			}
		}
		$product = DB::table("product")->where(["nydel" => "", "id" => $proid])->first(["name", "smalinfor", "webcontext", "number", "uid", "id", "image"]);
		$price = $this->productlist([$product->uid])[0]->price;
		$pro_ele = DB::table("pro_ele")->where(["nydel" => "", "product" => $product->uid])->get(["element"])->map(function ($item) {
			$ele = DB::table("element")->where("uid", $item->element)->first(["name"]);
			return ["name" => $ele->name];
		});
		$pro_package = DB::table("pro_package")->where(["nydel" => "", "belong_product" => $product->uid])->get(["product"])->map(function ($item) {
			$pros = DB::table("product")->where("uid", $item->product)->first(["name"]);
			return ["name" => $pros->name];
		});
		$product->ele = $pro_ele;
		$product->package = $pro_package;
		$product->price = $price;
		$result->put("product", $product);
		return $result;
	}
	public function getreceiptmoney($productlist) {
		//65 31 *23*3
		//80 31*22.8*10.3
		//110 39.5*27*23
		$col = collect([]);

		$group = collect($productlist)->groupBy("product")->map(function ($item, $keys) {
			return [$keys => $item->sum("number")];
		})->values()->collapse();

		$keys = $group->keys();
		DB::table("product as a")->where(["nydel" => ""])->where("bigsmall", "!=", "")->whereIn("uid", $keys)->get()->each(function ($item) use ($col) {
			$bigsmall = explode("*", $item->bigsmall);
			//x*y*z
			$tmp = ["x" => $bigsmall[0], "y" => $bigsmall[1], "z" => $bigsmall[2], "PreShip" => $item->PreShip, "Pro" => $item->uid];
			$col->push($tmp);
		});
		$ks = collect();
		$col->each(function ($item) use ($group, $ks) {
			// dd($item);
			for ($i = 0; $i < $group[$item['Pro']]; $i++) {
				$ks->push($item);
			};
		});
		$box = ["x" => 0, "y" => 0, "z" => 0];
		$tumb = collect(["x" => 65, "y" => 80, "z" => 110]);
		$hy = 0;
		$tkl = ["x" => true, "y" => true, "z" => true];
		$error = 0;

		while ($ks->count() > 0) {
			$ka = $ks->shift();
			$hy += $ka["z"];
			if ($hy < 3) {
				if ($tkl["x"] == true) {
					$box["x"]++;
					$tkl["x"] = false;
				}
			} elseif ($hy < 10.3) {
				if ($tkl["y"] == true) {
					$box["y"]++;
					($box["x"] > 0 && !$tkl["x"]) ? $box["x"]-- : "";
					$tkl["y"] = false;
				}
			} elseif ($hy < 46) {
				if ($tkl["z"] == true) {
					$box["z"]++;
					($box["y"] > 0 && !$tkl["y"]) ? $box["y"]-- : "";
					$tkl["z"] = false;
				}
			} elseif ($hy > 46) {
				$ks->push($ka);
				$hy = 0;
				$tkl = ["x" => true, "y" => true, "z" => true];
			}
		}
		$result = collect(["totel" => 0]);

		collect($box)->each(function ($item, $keys) use ($result, $tumb) {
			$result["totel"] += $tumb[$keys] * $item;
		});

		return $result["totel"];
	}
	public function getCartPro(Request $request) {
		// return $request->all();
		$tp = collect($request["proid"])->filter(function ($item) {return $item["kid"] == "main";});
		$dataaddpro = collect($request["proid"])->filter(function ($item) {return $item["kid"] == "addpro";});
		$addproinfo = DB::table("addproduct")->whereIn("id", explode(",", $dataaddpro->implode("id", ",")))->get(["name", "belong_pro", "id", "uid", "price", "product_id", "coded"]);
		$pro = explode(",", $tp->implode("id", ","));
		$countproduct = collect();
		$lka = DB::table("product")->whereIn("id", $pro)->get(["uid"])->map(function ($item) {return $item->uid;});
		$loa = $this->productlist($lka)->map(function ($item) use ($tp, $countproduct) {
			$pro = $tp->filter(function ($items) use ($item) {
				return $items['id'] == $item->id;
			})->first();
			$item->number = (isset($pro['number'])) ? $pro['number'] : 1;
			$item->kid = "main";
			$countproduct->push(["product" => $item->uid, "number" => $item->number]);
			return $item;
		});
		$addproinfo->each(function ($item) use ($dataaddpro, $loa, $countproduct) {
			$number = $dataaddpro->filter(function ($items) use ($item) {
				return $items['id'] == $item->id;
			})->first();
			if ($number) {
				$procode = DB::table("product")->where(["nydel" => "", "uid" => $item->belong_pro])->first(["outcode"])->outcode;
				$tmpa = (Object) ["uidcode" => $procode . "-" . $item->coded, "id" => $item->id, "name" => "(加購)" . $item->name, "uid" => $item->uid, "price" => $item->price, "group" => $item->belong_pro, "number" => (isset($number["number"])) ? $number["number"] : 1, "kid" => "addpro", "pricekid" => ""];
				collect(explode(",", $item->product_id))->each(function ($item) use ($countproduct, $number) {
					$countproduct->push(["product" => $item, "number" => (isset($number["number"])) ? $number["number"] : 1]);
				});
				$loa->push($tmpa);
			}

		});

		$addpro = collect();
		$loa->each(function ($item) use ($addpro) {
			if (isset($item->group)) {return;}
			$tmppro = DB::table("addproduct")->where(["nydel" => "", "belong_pro" => $item->uid])->get(["id", "uid", "coded", "name", "belong_pro", "product_id", "price"])->map(function ($items) use ($item) {
				$items->pro = DB::table("product")->whereIn("uid", explode(",", $items->belong_pro))->get(["name"])->implode("name", "/");
				$items->uidcode = $item->uidcode . "-" . $items->coded;
				return $items;
			})->filter(function ($item) {return $item->price > 0;});
			$addpro->push($tmppro);
		});
		$addpro = $addpro->collapse();
		$col = $this->getreceiptmoney($countproduct);
		return ["cartlist" => $loa, "addlist" => $addpro, "divary" => $col];
	}
	public function changedelvary(Request $request) {
		$op = $this->getCartPro($request)["divary"];
		return $op;
	}
	public function submitorder(Request $request) {
		$member = Auth::guard("api")->user();
		$datainfor = collect($request["infor"]);
		$datareceipt = collect($request["receipt"]);
		$listpro = collect($request['listpro']);
		$listpro = $listpro->map(function ($item) {
			$kl = ["name" => $item['name'],
				"number" => $item['quantity'],
				"off" => 0,
				"outpot" => "RMD1611160001",
				"price" => $item['price'],
				"pricekid" => $item['pricekid'],
				"uid" => $item['uid'],
				"uidcode" => $item['uidcode'],
				"totel" => $item['price'] * $item['quantity']];
			if (isset($item['group'])) {
				$kl['belong'] = $item["group"];
			}
			return $kl;
		});
		if ($request['delivery']["type"] != "LocalGet") {
			$delivery = $request['delivery'];
			$listpro->push(
				[
					"name" => $delivery['fullname'],
					"number" => 1,
					"off" => 0,
					"outpot" => "RMD1611160001",
					"price" => $delivery['price'],
					"pricekid" => "delivery",
					"uid" => "delivery",
					"uidcode" => "delivery",
					"totel" => $delivery['price'],
					"belong" => "delivery",
				]);
			// $kl['belong'] = $item["group"];
		}
		$totelmoney = $listpro->sum("totel");
		// dd($listpro);
		$today = Carbon::now('Asia/Taipei');
		$infor = [
			"account" => ($member) ? $member->account : "",
			"areacode" => $datainfor['post_id'],
			"email" => $datainfor["email"],
			"homephone" => $datainfor['phone'],
			"location" => $datainfor['address'],
			"name" => $datainfor['name'],
			"order_date" => $today->toDateString(),
			"phone" => $datainfor['cellphone'],
			"uid" => ($member) ? $member->uid : "",
		];
		$receipt = [
			"receipt_account" => ($member) ? $member->account : "",
			"receipt_areacode" => $datareceipt['post_id'],
			"receipt_email" => $datareceipt['email'],
			"receipt_homephone" => $datareceipt['phone'],
			"receipt_id" => ($member) ? $member->id : "",
			"receipt_location" => $datareceipt['address'],
			"receipt_name" => $datareceipt['name'],
			"receipt_order_date" => $today,
			"receipt_phone" => $datareceipt['cellphone'],
			"receipt_uid" => ($member) ? $member->uid : "",
		];

		$tmpa = [
			"belong_bill_state" => "BST1611170001",
			"infor" => $infor,
			"order_state" => "OTP1611160001",
			"receipt" => $receipt,
			"totel_money" => $totelmoney,
			// "fii" => "Sdf",
			"listpro" => $listpro->all(),
			"outcargo" => [],
		];
		// return $tmpa;
		$local = env('ERP_API', 'mysql');
		$Http = new Client(['base_uri' => $local]);

		$res = $Http->request("POST", "Ordersend", ["form_params" => ["data" => $tmpa], "headers" => null]);
		return $res->getBody();

	}

}
