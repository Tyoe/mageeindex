<?php
namespace App\Http\Controllers\PublicCtrl;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Response;
trait Supper {

	private function updateLog($table, $active, $data, $uid) {
		try {
			DB::beginTransaction();
			$sourcedata = DB::table($table)->where($uid)->get();
			if ($sourcedata->count() > 0) {
				$mytime = Carbon::now("Asia/Taipei");
				$user = Auth::guard("api")->user()->uid;
				$data = collect($data);
				$data->forget("uid", "updatatime");

				$sourcedata->each(function ($items, $keyss) use ($data, $mytime, $user, $table, $active) {
					$id = $items->id;
					$resolut = collect(["after" => collect(), "before" => collect()]);
					$data->each(function ($item, $keys) use ($items, $resolut, $mytime, $user, $id, $table, $active) {
						if ($item != $items->$keys && $keys != "updatatime") {
							$resolut['before']->put($keys, urlencode($items->$keys));
							$resolut['after']->put($keys, urlencode($item));
						}
					});
					if ($resolut['before']->count() > 0) {
						DB::table("TableLogInfor")->insert(["uptable" => $table, "updateid" => $id, "upbefore" => urldecode($resolut['before']->toJson()), "upafter" => urldecode($resolut['after']->toJson()), "uptype" => $active, "createtime" => $mytime, "cmember" => 0, "remark" => $user]);
					}
				});
				$data->put("updatatime", $mytime);
				// dd($data->all());
				DB::table($table)->where($uid)->update($data->all());
				DB::commit();
				return DB::table($table)->where($uid)->get();
			}
		} catch (\Exception $e) {
			DB::rollback();
			dd(Response::HTTP_INTERNAL_SERVER_ERROR);
			return;
		}
	}
	private function insertrefrsh($table, $data) {
		try {
			DB::beginTransaction();
			$user = Auth::guard("api")->user()->id;
			$mytime = Carbon::now('Asia/Taipei');
			$data["createtime"] = $mytime->toDateTimeString();
			$uid = $this->uid($table);
			// dd($data);
			$data["uid"] = 0;
			$data['cmember'] = $user;
			$data = $data;
			// dd($data);
			$dk = DB::table($table)->insert($data);
			DB::commit();
			return $newinsertdata = DB::table($table)->where("uid", $uid)->get();
		} catch (\Exception $e) {
			DB::rollback();
			dd(Response::HTTP_INTERNAL_SERVER_ERROR);
			return;
		}
	}
}
