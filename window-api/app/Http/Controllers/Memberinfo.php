<?php

namespace App\Http\Controllers;

use App\postcode;
use Auth;
use DB;
use Illuminate\Http\Request;

class Memberinfo extends Controller {
	use PublicCtrl\Supper;
	public function editpwd(Request $request) {
		$data = $request["data"];
		$apitoken = Auth::guard("api")->user()->api_token;
		$status = "OK";
		if ($data["newpw"] == $data['repw']) {
			$ki = $this->updateLog("member", "updatepwd", ["password" => $data['newpw']], ["api_token" => $apitoken]);
			if (!$ki) {
				$status = "Error";
			}
		} else {
			$status = "Passwordnochecked";
		}

		return ["status" => $status];
	}
	public function getmemberinfo() {
		$member = Auth::guard("api")->user();
		$postcode = postcode::where("area_uid", $member->postelcode)->first(["area_uid", "area_name", "city_name"]);
		$updateLog = DB::table("TableLogInfor")->where(["uptable" => "member", "updateid" => $member->id, "uptype" => "updatepwd"])->orderBy("createtime", "desc")->first(["createtime"]);
		return $result = [
			"name" => $member->name,
			"userid" => $member->account,
			"pwupdate" => ($updateLog) ? $updateLog->createtime : "0000-00-00",
			"subnews" => ($member->epapar == "Y"),
			"signdate" => $member->createtime,
			"homephone" => $member->homephone,
			"sex" => $member->gender_id,
			"birthday" => $member->birth,
			"phone" => $member->homephone,
			"cellphone" => $member->phone,
			"email" => $member->email,
			"location" => $member->location,
			"postelcode" => $member->postelcode,
			"school" => $member->school . "/" . $member->department, "work" => $member->work,
			"post" => ($postcode) ? $postcode->area_uid . "/" . $postcode->city_name . "/" . $postcode->area_name : "",
			"add" => $member->location,
			"skill" => ""];
	}
}
