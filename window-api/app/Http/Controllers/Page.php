<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;

class Page extends Controller {
	public function getDocsGroupList() {
		return DB::table("public_class")->where(["nydel" => "", "func" => "DocsGroup"])->get(["name", "values"]);
	}
	public function getDocList(Request $request) {
		$Group = $this->getDocsGroupList();
		$Docslist = DB::table("DocsContent")->where(["nydel" => "", "DocsTag" => $request['group']])->get(["name", "content_s", "id", "uid"]);
		return ["Group" => $Group, "Docslist" => $Docslist];
	}
	public function getDocsInfor(Request $request) {
		$Group = $request['group'];
		$doc = $request['doc'];
		$Gr = DB::table("public_class")->where(["nydel" => "", "func" => "DocsGroup", "values" => $Group])->first(["name", "values"]);
		$Docinfo = DB::table("DocsContent")->where(["nydel" => "", "id" => $doc])->first(["name", "id", "content", "content_s"]);
		$DocTag = DB::table("DocsTag")->where(["nydel" => "", "DocsGroup" => $doc])->get(["id", "name", "remark"]);
		return ["Group" => $Gr, "Docinfo" => $Docinfo, "DocTag" => $DocTag];
	}
	public function getIndex() {
		$News = DB::table("WebNews")->where(["nydel" => "", "isonline" => "1"])->orderBy("orders", "asc")->get(["name", "url"]);
		$Banner = DB::table("WebBanner")->where(["nydel" => "", "isonline" => "1"])->orderBy("orders", "asc")->get(["name", "image", "id", "backcolor", "url"]);
		$Content = DB::table("WebContent")->where(["nydel" => "", "isonline" => "1"])->orderBy("orders", "asc")->get(["name", "content", "url", "id", "image"]);
		$WebFunclist = DB::table("WebFunclist")->where(["nydel" => "", "isonline" => "1"])->orderBy("orders", "asc")->get(["name", "content", "image", "url"]);
		return ["Banner" => $Banner, "News" => $News, "Content" => $Content, "WebFunclist" => $WebFunclist];
	}
	public function getAd() {
		$AD = DB::table("WebPage")->where(["nydel" => "", "isonline" => "1", "func" => "ad"])->orderBy("orders", "asc")->first(["url", "image"]);
		return ($AD) ? $AD : "";
	}
}
