<?php
namespace App\Http\Controllers;

use Auth;
use Carbon\Carbon;
use DB;
use Hash;
use Illuminate\Http\Request;

class Login extends Controller {
	public function Login(Request $request) {
		$account = $request["account"];
		$password = $request['password'];
		$member = DB::table("member")->where(["account" => $account, "password" => $password, "nydel" => ""])->first(["api_token", "open", "email", "name"]);
		$result = collect();
		if ($member) {
			if ($member->open == "Y") {
				if ($member->api_token == "") {
					$mytime = Carbon::now('Asia/Taipei');
					$apitoken = $request['account'] . $request['passsword'] . $mytime;
					$hash = Hash::make($apitoken);
					DB::table("member")->where(["account" => $account, "password" => $password, "nydel" => ""])->update(["api_token" => $hash]);
					$member = DB::table("member")->where(["account" => $account, "password" => $password, "nydel" => ""])->first(["api_token", "open", "email", "name"]);
				};
				$result->put("member", $member);
				$result->put("status", "open");
			} else {
				$result->put("status", "no-open");
			};
		} else {
			$result->put("status", "nojoinmember");
		}
		return $result->all();
	}
	public function checkedmember(Request $reqeust) {
		$id = Auth::guard("api")->user()->api_token;
		$member = DB::table("member")->where(["api_token" => $id])->first(["api_token", "open", "email", "name", "account", "id", "uid", "postelcode", "phone", "homephone", "location"]);

		return ["Member" => $member];
	}
	public function getpost() {
		return DB::table("post_code")->where(["nydel" => ""])->get(["area_uid", "area_name", "city_name"]);
	}
	public function simpleSQL($table, $where) {
		return $DB = DB::table($table)->where(["nydel" => ""])->where($where)->orderBy("values", "asc")->get();
	}
	public function getjoindata() {
		$post = $this->getpost();
		$education = $this->simpleSQL("public_class", ["func" => "education"]);
		$job = $this->simpleSQL("public_class", ["func" => "job"]);
		$work_state = $this->simpleSQL("public_class", ["func" => "work_state"]);
		return ["post" => $post, "education" => $education, "job" => $job, "work_state" => $work_state];
	}
	public function joinmagee(Request $request) {
		// return $request->all();
		$data = $request->all();
		// DB::beginTransaction();
		$kl = [
			"identity" => "1",
			"open" => "",
			"name" => $data['member_name'],
			"account" => $data['member_id'],
			"email" => $data['member_id'],
			"password" => $data["member_pw"],
			"phone" => ($data["cellphone"]) ? $data["cellphone"] : "",
			"homephone" => ($data["phone"]) ? $data["phone"] : "",
			"location" => ($data["add"]) ? $data["add"] : "",
			"postelcode" => ($data["post_id"]) ? $data["post_id"] : "",
			"radcount" => "200",
			"department" => ($data["department"]) ? $data["department"] : "",
			"gender_id" => ($data["sex"]) ? $data["sex"] : "",
			"birth" => $data['brithday']['year'] . "-" . $data['brithday']['month'] . "-" . $data['brithday']['day'],
			"work" => ($data["Career"]) ? $data["Career"] : "",
			"epapar" => ($data["subnews"]) ? 1 : 0,
			"member_edu" => ($data["education"]) ? $data["education"] : "",
			"member_level" => "9",
			"worktype" => ($data["job"]) ? $data["job"] : "",
			"createtime" => Carbon::now('Asia/Taipei'),
		];
		DB::table("member")->insert($kl);
		// DB::commit();
		return $kl;
	}
	public function checkmember($memberuid) {

	}
}
