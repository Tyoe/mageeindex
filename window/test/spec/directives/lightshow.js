'use strict';

describe('Directive: lightshow', function () {

  // load the directive's module
  beforeEach(module('mageeWindowApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<lightshow></lightshow>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the lightshow directive');
  }));
});
