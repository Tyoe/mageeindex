'use strict';

describe('Directive: trantextarea', function () {

  // load the directive's module
  beforeEach(module('mageeWindowApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<trantextarea></trantextarea>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the trantextarea directive');
  }));
});
