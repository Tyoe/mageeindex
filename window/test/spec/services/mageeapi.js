'use strict';

describe('Service: mageeapi', function () {

  // load the service's module
  beforeEach(module('mageeWindowApp'));

  // instantiate service
  var mageeapi;
  beforeEach(inject(function (_mageeapi_) {
    mageeapi = _mageeapi_;
  }));

  it('should do something', function () {
    expect(!!mageeapi).toBe(true);
  });

});
