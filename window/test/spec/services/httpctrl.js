'use strict';

describe('Service: httpctrl', function () {

  // load the service's module
  beforeEach(module('mageeWindowApp'));

  // instantiate service
  var httpctrl;
  beforeEach(inject(function (_httpctrl_) {
    httpctrl = _httpctrl_;
  }));

  it('should do something', function () {
    expect(!!httpctrl).toBe(true);
  });

});
