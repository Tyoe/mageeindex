'use strict';

describe('Service: Member/member', function () {

  // load the service's module
  beforeEach(module('mageeWindowApp'));

  // instantiate service
  var Member/member;
  beforeEach(inject(function (_Member/member_) {
    Member/member = _Member/member_;
  }));

  it('should do something', function () {
    expect(!!Member/member).toBe(true);
  });

});
