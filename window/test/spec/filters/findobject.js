'use strict';

describe('Filter: findObject', function () {

  // load the filter's module
  beforeEach(module('mageeWindowApp'));

  // initialize a new instance of the filter before each test
  var findObject;
  beforeEach(inject(function ($filter) {
    findObject = $filter('findObject');
  }));

  it('should return the input prefixed with "findObject filter:"', function () {
    var text = 'angularjs';
    expect(findObject(text)).toBe('findObject filter: ' + text);
  });

});
