'use strict';

describe('Filter: trantextarea', function () {

  // load the filter's module
  beforeEach(module('mageeWindowApp'));

  // initialize a new instance of the filter before each test
  var trantextarea;
  beforeEach(inject(function ($filter) {
    trantextarea = $filter('trantextarea');
  }));

  it('should return the input prefixed with "trantextarea filter:"', function () {
    var text = 'angularjs';
    expect(trantextarea(text)).toBe('trantextarea filter: ' + text);
  });

});
