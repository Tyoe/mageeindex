'use strict';

describe('Filter: modgroup', function () {

  // load the filter's module
  beforeEach(module('mageeWindowApp'));

  // initialize a new instance of the filter before each test
  var modgroup;
  beforeEach(inject(function ($filter) {
    modgroup = $filter('modgroup');
  }));

  it('should return the input prefixed with "modgroup filter:"', function () {
    var text = 'angularjs';
    expect(modgroup(text)).toBe('modgroup filter: ' + text);
  });

});
