'use strict';

describe('Controller: HeaderHeaderCtrl', function () {

  // load the controller's module
  beforeEach(module('mageeWindowApp'));

  var HeaderHeaderCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    HeaderHeaderCtrl = $controller('HeaderHeaderCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(HeaderHeaderCtrl.awesomeThings.length).toBe(3);
  });
});
