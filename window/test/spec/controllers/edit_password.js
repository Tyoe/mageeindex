'use strict';

describe('Controller: EditPasswordCtrl', function () {

  // load the controller's module
  beforeEach(module('mageeWindowApp'));

  var EditPasswordCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    EditPasswordCtrl = $controller('EditPasswordCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(EditPasswordCtrl.awesomeThings.length).toBe(3);
  });
});
