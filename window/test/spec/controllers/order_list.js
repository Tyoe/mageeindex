'use strict';

describe('Controller: OrderListCtrl', function () {

  // load the controller's module
  beforeEach(module('mageeWindowApp'));

  var OrderListCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    OrderListCtrl = $controller('OrderListCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(OrderListCtrl.awesomeThings.length).toBe(3);
  });
});
