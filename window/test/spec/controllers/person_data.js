'use strict';

describe('Controller: PersonDataCtrl', function () {

  // load the controller's module
  beforeEach(module('mageeWindowApp'));

  var PersonDataCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PersonDataCtrl = $controller('PersonDataCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(PersonDataCtrl.awesomeThings.length).toBe(3);
  });
});
