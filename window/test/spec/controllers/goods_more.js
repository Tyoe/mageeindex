'use strict';

describe('Controller: GoodsMoreCtrl', function () {

  // load the controller's module
  beforeEach(module('mageeWindowApp'));

  var GoodsMoreCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    GoodsMoreCtrl = $controller('GoodsMoreCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(GoodsMoreCtrl.awesomeThings.length).toBe(3);
  });
});
