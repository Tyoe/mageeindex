'use strict';

describe('Controller: GoodsClassCtrl', function () {

  // load the controller's module
  beforeEach(module('mageeWindowApp'));

  var GoodsClassCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    GoodsClassCtrl = $controller('GoodsClassCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(GoodsClassCtrl.awesomeThings.length).toBe(3);
  });
});
