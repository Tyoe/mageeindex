'use strict';

describe('Controller: GoodsBookCtrl', function () {

  // load the controller's module
  beforeEach(module('mageeWindowApp'));

  var GoodsBookCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    GoodsBookCtrl = $controller('GoodsBookCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(GoodsBookCtrl.awesomeThings.length).toBe(3);
  });
});
