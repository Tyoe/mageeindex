'use strict';

describe('Controller: LoginPageCtrl', function () {

  // load the controller's module
  beforeEach(module('mageeWindowApp'));

  var LoginPageCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    LoginPageCtrl = $controller('LoginPageCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(LoginPageCtrl.awesomeThings.length).toBe(3);
  });
});
