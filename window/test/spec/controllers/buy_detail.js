'use strict';

describe('Controller: BuyDetailCtrl', function () {

  // load the controller's module
  beforeEach(module('mageeWindowApp'));

  var BuyDetailCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    BuyDetailCtrl = $controller('BuyDetailCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(BuyDetailCtrl.awesomeThings.length).toBe(3);
  });
});
