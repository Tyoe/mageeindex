'use strict';

/**
 * @ngdoc filter
 * @name mageeWindowApp.filter:sum
 * @function
 * @description
 * # sum
 * Filter in the mageeWindowApp.
 */
angular.module('mageeWindowApp')
    .filter('sum', function() {
        return function(input, attr) {
            var totel = 0;
            if (input) {
                input.map(function(item, keys) {
                    item[attr] = parseInt(item[attr]);
                    totel += item[attr];
                })
            }
            return totel;
        };
    });
