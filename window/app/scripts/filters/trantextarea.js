'use strict';

/**
 * @ngdoc filter
 * @name mageeWindowApp.filter:trantextarea
 * @function
 * @description
 * # trantextarea
 * Filter in the mageeWindowApp.
 */
angular.module('mageeWindowApp')
  .filter('trantextarea', function () {
    return function (input) {
    	
      return input.replace(/\r/g,"</br>");
    };
  });
