'use strict';

/**
 * @ngdoc filter
 * @name mageeWindowApp.filter:findObject
 * @function
 * @description
 * # findObject
 * Filter in the mageeWindowApp.
 */
angular.module('mageeWindowApp')
    .filter('findObject', function() {
        return function(input, attr) {
            var index = -1;
            input.map(function(item, keys) {
                var tio = true;
                angular.forEach(attr, function(items, keya) {
                    if (item[keya] == items && tio==true) {
                        tio = true;
                    } else {
                        tio = false;
                    }
                });
                if (tio) {
                    index = keys;
                }
            });

            return index;
        };
    });
