'use strict';

/**
 * @ngdoc filter
 * @name mageeWindowApp.filter:modgroup
 * @function
 * @description
 * # modgroup
 * Filter in the mageeWindowApp.
 */
angular.module('mageeWindowApp')
    .filter('modgroup', function() {
        return function(input, attr) {
            var index = attr.index;
            var number = attr.number;
            var length = input.length;
            var tmpdata = {};
            var datalist = [];
            tmpdata[index] = [];
            input.forEach(function(item, keys) {
                if (keys % number == 0 && keys > 0) {
                    datalist.push(angular.copy(tmpdata));
                    tmpdata[index] = [];
                }
                tmpdata[index].push(item);
            });
            if(tmpdata[index].length>0){
            	datalist.push(tmpdata);
            }
            console.log(datalist);

            return datalist;
        };
    });
