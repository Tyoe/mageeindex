'use strict';

/**
 * @ngdoc overview
 * @name mageeWindowApp
 * @description
 * # mageeWindowApp
 *
 * Main module of the application.
 */
angular
    .module('mageeWindowApp', [
        'ngAnimate',
        'ngCookies',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ngTouch',
        'ui.router',
        'ui.bootstrap',
        'satellizer',
    ])
    .config(function($stateProvider, $urlRouterProvider, $httpProvider, $locationProvider) {
        $urlRouterProvider.otherwise('/');
        $stateProvider.state("index", {
            url: "/",
            controller: "MainCtrl",
            controllerAs: "main",
            templateUrl: "views/main.html"
        }).state("class", {
            url: "/class/:pro_class",
            controller: "GoodsClassCtrl",
            controllerAs: "gcc",
            templateUrl: "views/goods_class.html"
        }).state("book", {
            url: "/book/:pro_book",
            controller: "GoodsBookCtrl",
            controllerAs: "gbc",
            templateUrl: "views/goods_book.html"
        }).state("member", {
            url: "/joinmember",
            controller: "MemberCtrl",
            controllerAs: "mbc",
            templateUrl: "views/member.html"
        }).state("changepw", {
            url: "/member/changepw",
            controller: "EditPasswordCtrl",
            controllerAs: "epc",
            templateUrl: "views/page/changepw.html"
        }).state("person", {
            url: "/member/person",
            controller: "PersonDataCtrl",
            controllerAs: "pdc",
            templateUrl: "views/page/person_data.html"
        }).state("shopcart", {
            url: "/shopcart",
            controller: "BuyDetailCtrl",
            controllerAs: "bdc",
            templateUrl: "views/page/buy_detail.html"
        }).state("goodsmore", {
            url: "/prouduct/goodsmore/:proid/:caseid",
            controller: "GoodsMoreCtrl",
            controllerAs: "gmc",
            templateUrl: "views/page/gd_more.html"
        })
        // .state("about", {
        //     url: "/news/doc4/1",
        //     controller: "MainCtrl",
        //     controllerAs: "main",
        //     templateUrl: "views/page/about.html"
        // }).state("teacher", {
        //     url: "/news/doc4/3",
        //     controller: "TeacherCtrl",
        //     controllerAs: "tc",
        //     templateUrl: "views/page/teacher.html"
        // })
        .state("news", {
            url: "/docs/:group",
            controller: "NewsListCtrl",
            controllerAs: "nlc",
            templateUrl: "views/news_list.html"
        }).state("newsdoc", {
            url: "/newss/:listid/:docid",
            controller: "NewsCtrl",
            controllerAs: "nc",
            templateUrl: "views/page/news_doc.html"
        }).state("order", {
            url: "/member/order",
            controller: "OrderListCtrl",
            controllerAs: "olc",
            templateUrl: "views/page/order_list.html"
        }).state("outlogin",{
            onEnter:function ($auth,$state,$location){
              $auth.removeToken();
           
              $state.go("index");
            }
        });
    }).config(function($sceDelegateProvider, API, $httpProvider, $authProvider) {
        API = (API.search(/http/g) > -1) ? API : "http://www.magee.tw/magee-api/" + API;
        $authProvider.loginUrl = API + "Login";
        $authProvider.tokenName = 'api_token';
        $sceDelegateProvider.resourceUrlWhitelist([
            // Allow same origin resource loads.
            'self',
            // Allow loading from our assets domain.  Notice the difference between * and **.
            '**'
        ]);
        $httpProvider.defaults.useXDomain = true;
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
        $httpProvider.defaults.useXDomain = true;
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
    }).run(function($rootScope, Member) {
        $rootScope.$watch(function() {
            return localStorage['satellizer_api_token'];
        }, function(data) {
            if (data) {
                Member.checkmember().then(function(data) {
                    $rootScope.member = data.data.Member;
                },function (error){
                    console.log(error.status);
                });
            }else{
              $rootScope.member=data;
            }
        });

    });
