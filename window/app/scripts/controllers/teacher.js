'use strict';

/**
 * @ngdoc function
 * @name mageeWindowApp.controller:TeacherCtrl
 * @description
 * # TeacherCtrl
 * Controller of the mageeWindowApp
 */
angular.module('mageeWindowApp')
  .controller('TeacherCtrl', function () {
  	var vm=this;
  	vm.teacher=[{name:"1名字",skill1:"全能講師",skill2:"實務及法規專長",img:"images/teacher-1.png",
  				intro:"國際專業領隊\r20年以上資歷\r\r陸客專業導遊\r馬跡中心班主任\r達跡旅行社總經理\r華語導遊證照\r國際領隊證照\r旅行業經理人執照\r"},
  				{name:"2名字",skill1:"全能講師",skill2:"實務及法規專長",img:"images/goods-normal2.png",
  				intro:"國際專業領隊\r20年以上資歷\r\r陸客專業導遊\r馬跡中心班主任\r達跡旅行社總經理\r華語導遊證照\r國際領隊證照\r旅行業經理人執照\r"},
  				{name:"3名字",skill1:"全能講師",skill2:"實務及法規專長",img:"images/teacher-3.png",
  				intro:"國際專業領隊\r20年以上資歷\r\r陸客專業導遊\r馬跡中心班主任\r達跡旅行社總經理\r華語導遊證照\r國際領隊證照\r旅行業經理人執照\r"},
  				{name:"4名字",skill1:"全能講師",skill2:"實務及法規專長",img:"images/goods-normal.png",
  				intro:"國際專業領隊\r20年以上資歷\r\r陸客專業導遊\r馬跡中心班主任\r達跡旅行社總經理\r華語導遊證照\r國際領隊證照\r旅行業經理人執照\r"},
  				{name:"5名字",skill1:"全能講師",skill2:"實務及法規專長",img:"images/goods-normal.png",
  				intro:"國際專業領隊\r20年以上資歷\r\r陸客專業導遊\r馬跡中心班主任\r達跡旅行社總經理\r華語導遊證照\r國際領隊證照\r旅行業經理人執照\r"},
  				{name:"6名字",skill1:"全能講師",skill2:"實務及法規專長",img:"images/goods-normal.png",
  				intro:"國際專業領隊\r20年以上資歷\r\r陸客專業導遊\r馬跡中心班主任\r達跡旅行社總經理\r華語導遊證照\r國際領隊證照\r旅行業經理人執照\r"}]
  });
