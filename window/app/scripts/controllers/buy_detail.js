'use strict';

/**
 * @ngdoc function
 * @name mageeWindowApp.controller:BuyDetailCtrl
 * @description
 * # BuyDetailCtrl
 * Controller of the mageeWindowApp
 */
angular.module('mageeWindowApp')
    .controller('BuyDetailCtrl', function(Product, $filter, $scope, Member, mageeapi,$uibModal) {
        var vm = this;
        vm.orderuid = "";
        vm.active = 0;
        vm.cart = (localStorage['cart']) ? JSON.parse(localStorage['cart']) : [];
        Product.getCartPro({ proid: vm.cart }).then(function(data) {
            vm.shopcart = data.data.cartlist.map(function(item) {
                var max = 5;
                if (item.kid == "addpro") {
                    var filt = $filter("filter")(data.data.cartlist, { kid: "main", uid: item.group });
                    max = (filt.length > 0) ? filt[0].number : max;
                }
                return {  uidcode: item.uidcode, id: item.id, name: item.name, price: item.price, quantity: item.number, uid: item.uid, kid: item.kid, group: item.group, max: max, pricekid: (item.pricekid) ? item.pricekid : "NO" };
            });
            vm.additem = data.data.addlist.map(function(item) {
                console.log(item);
                return { uidcode:item.uidcode,pricekid: "NO",id: item.id, uid: item.uid, name: "(加購)" + item.name, price: item.price, group: item.belong_pro, quantity: 1 }
            });
            vm.divary = data.data.divary;
            vm.buy_way = [{ fullname: "現場領取", price: 0, checked: false, type: "LocalGet" }, { name: "運費", fullname: "郵寄(運費另計:" + vm.divary + ")", price: vm.divary, checked: true, type: "PostOff" }, { name: "貨到付款", fullname: "貨到付款 (運費之外郵局另酌收手續費$30元)", price: vm.divary + 30, checked: false, type: 'SendAfterPay' }];
            vm.delivery = vm.buy_way[1];
        });
        vm.buy_data = [];
        vm.buy_data.company = [];
        Member.getmemberinfo().then(function(data) {
            vm.buy_data.order = {
                name: data.data.name,
                phone: data.data.homephone,
                cellphone: data.data.phone,
                email: data.data.email,
                post_id: data.data.postelcode,
                address: data.data.location
            }
        });
        mageeapi.getpost().then(function(data) {
            vm.post_data = data.data.map(function(item) {
                return { id: item.area_uid, name: item.city_name, area: item.area_name };
            });

        });
        vm.sysorder = function() {
            vm.buy_data.receipt = angular.copy(vm.buy_data.order);
        };
        vm.active = 0;
        vm.saledate = '2016-08-24';
        vm.delivery = { name: "", price: 0 };
        vm.nextstep = function(nextnum) {
            $("body").animate({ scrollTop: $("#step" + nextnum).offset().top - 100 }, "slow");
        }



        // vm.buy_data = { name: vm.member.name, phone: vm.member.homephone, cellphone: vm.member.phone, email: vm.member.email,
        //  post_id: vm.member.postelcode, address: vm.member.location }

        vm.get_data = { name: "", phone: "", cellphone: "", email: "", post_id: "", address: "" }
        vm.company_data = { number: "", fullname: "", post_id: "", address: "" }

        vm.ordernumber = "ORDER1703200001"
        vm.samecheck = { get: false, comp: false };
        vm.checked = function() {
            if (vm.samecheck.get == true) {
                vm.get_data = vm.buy_data;
            } else {
                vm.get_data = { name: "", phone: "", cellphone: "", email: "", post_id: "", address: "" }
            }
        }
        vm.companychecked = function() {
            if (vm.samecheck.comp == true) {
                vm.company_data = vm.get_data;
            } else {
                vm.company_data = { number: "", name: "", post_id: "", address: "" }
            }
        }
        vm.addgoods = function(item) {
            console.log(item);
            var s = $filter("findObject")(vm.shopcart, { kid: "addpro", id: item.id });
            if (s > -1) {
                var cal = $filter("findObject")(vm.cart, { kid: "addpro", id: item.id });
                vm.cart.splice(cal, 1);
                vm.shopcart.splice(s, 1);
            } else {
                var prodnumber = $filter("filter")(vm.shopcart, { kid: "main", uid: item.group })[0].quantity;
                var io = {"uidcode":item.uidcode, uid:item.uid,pricekid: item.pricekid, id: item.id, kid: "addpro", name: item.name, price: item.price, group: item.group, quantity: 1, kid: "addpro", max: prodnumber }
                vm.cart.push({ "id": item.id, "number": 1, kid: "addpro" });
                vm.shopcart.push(io);
            }
            localStorage['cart'] = JSON.stringify(vm.cart);
            return;
        }
        vm.changenumber = function(item) {
            var index = $filter("findObject")(vm.cart, { id: item.id, kid: item.kid });
            vm.cart[index].number = item.quantity;
            if (item.kid == "main") {
                var fk = $filter("filter")(vm.shopcart, { kid: "addpro", group: item.uid });
                fk.map(function(items) {
                    items.max = item.quantity;
                    if (items.quantity > item.quantity) {
                        items.quantity = item.quantity;
                        var adcart = $filter("filter")(vm.cart, { kid: "addpro", id: items.id });
                        adcart.map(function(itemss) {
                            itemss.number = items.quantity;
                        });
                    }
                });
            }
            mageeapi.changedelvary({ proid: vm.cart }).then(function(data) {
                console.log(data);
                vm.buy_way[1] = { name: "運費", fullname: "郵寄(運費另計:" + data.data + ")", price: data.data, type: "PostOff" };
                vm.buy_way[2]={ name: "貨到付款", fullname: "貨到付款 (運費之外郵局另酌收手續費$30元)", price:  data.data + 30, checked: false, type: 'SendAfterPay'};
            });
            localStorage['cart'] = JSON.stringify(vm.cart);
        }
        vm.remove = function(item) {
            var index = $filter("findObject")(vm.cart, { id: item.id, kid: item.kid });
            var s = $filter("findObject")(vm.shopcart, { kid: item.kid, id: item.id });
            vm.cart.splice(index, 1);
            vm.shopcart.splice(s, 1);
            localStorage['cart'] = JSON.stringify(vm.cart);
        }
        vm.deliveryway = function(fee) {
            vm.delivery = fee;
            vm.buy_way.type = fee.type;
            if (fee.price == 30) {
                vm.delivery.name = "運費(" + fee.price + ")";
                // vm.delivery.price = fee.price;
            } else {
                // vm.delivery.name = fee.name;
                // vm.delivery.price = fee.price;
            }
        }
        vm.login = function() {
            var open = $uibModal.open({
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'views/login/page.html',
                controller: 'LoginPageCtrl',
                controllerAs: 'lpc',
                backdrop: "static",
                size: "md",
            });
            open.result.then(function(data) {
                console.log($scope);
            });
        }
        vm.buystep = function(stepnum) {
            if (vm.shopcart.length == 0) {
                return false;
            }
            if (vm.active == 5) {
                return false;
            } else {
                vm.active = stepnum;
            }
        }
        vm.syscomtitle = function() {
            vm.buy_data.company.post_id = vm.buy_data.order.post_id;
            vm.buy_data.company.address = vm.buy_data.order.address
        }
        vm.order_final = function(stepnum) {
            vm.active = stepnum;
        }
        vm.submitorder = function() {
            console.log(vm.buy_data.order);
            var data = { infor: vm.buy_data.order, receipt: vm.buy_data.receipt, company: vm.buy_data.company, listpro: vm.shopcart, delivery: vm.delivery };
            mageeapi.submitorder(data).then(function(data) {
                vm.orderuid=data.data;
                vm.active=5;
                vm.ch={delivery:vm.delivery,shopcart:vm.shopcart};
                vm.delivery=null;
                vm.shopcart=null;
                localStorage["cart"]="";
                alert("您已經完成本中心訂單，將會記送確認信函致您的訂購人Email ");
            });
        }

    });
