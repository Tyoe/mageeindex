'use strict';

/**
 * @ngdoc function
 * @name mageeWindowApp.controller:GoodsBookCtrl
 * @description
 * # GoodsBookCtrl
 * Controller of the mageeWindowApp
 */
angular.module('mageeWindowApp')
    .controller('GoodsBookCtrl', function($stateParams, Product, API_FILE, mageeapi) {
        var vm = this;
        mageeapi.getAd().then(function(data) {
            vm.ad = data.data.map(function(item) {
                item.image = (item.image) ? API_FILE + item.image : "";
                return $item;
            });
        });
        var ks = function() {
            Product.getBookPageList().then(function(data) {
                console.log(data);
                vm.course_class = data.data.ProClass.map(function(item) {
                    item.color = API_FILE + "../" + item.color;
                    item.product.map(function(items) {
                        items.image = API_FILE + "../" + items.image;
                        return items;
                    });
                    return item;
                });
                return;
                vm.course_class = data.data.ProClass.map(function(item) {
                    item.color = API_FILE + "../" + item.color;
                    return item;
                });
            });
        }
        ks();
        vm.test = function() {
            ks();
        }
        vm.class = "";
        var tmptext = "慶馬跡蟬聯105.104.103.102.101五年全國榜首\r定價 $2,400 / 最後優惠";
        vm.pro_book = ($stateParams.pro_book) ? $stateParams.pro_book : "1";
        vm.class_book = [{ id: 1, name: "考證用書", fullname: "導遊領隊考證用書", color: "deeprad" }, { id: 2, name: "帶團系列", fullname: "絕對帶團系列用書", color: "deepblue" }]
        vm.probooklist = [{
            id: 11,
            class: 1,
            img: "images/goods-normal.png",
            name: "絕對考上導遊+領隊考照有聲書",
            info: tmptext,
            price: 1420
        }, {
            id: 12,
            class: 1,
            img: "images/goods-normal.png",
            name: "絕對考上導遊+領隊 日語篇",
            info: tmptext,
            price: 330
        }, {
            id: 13,
            class: 1,
            img: "images/goods-normal2.png",
            name: "絕對考上導遊+領隊 英文篇",
            info: tmptext,
            price: 330
        }, {
            id: 14,
            class: 2,
            img: "images/goods-normal2.png",
            name: "帶團書",
            info: tmptext,
            price: 330
        }]
        vm.goto = function(data) {
            vm.class = data;
        }
    });
