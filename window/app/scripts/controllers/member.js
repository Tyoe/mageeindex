'use strict';

/**
 * @ngdoc function
 * @name mageeWindowApp.controller:MemberCtrl
 * @description
 * # MemberCtrl
 * Controller of the mageeWindowApp
 */
angular.module('mageeWindowApp')
    .controller('MemberCtrl', function(mageeapi) {
        var vm = this;
        var date=new Date();
        vm.member = {
            member_id: "",
            member_pw: "",
            member_name: "",
            subnews: "",
            sex: "",
            brithday: {year:(date.getFullYear())-30,month:1,day:1},
            phone: "",
            cellphone: "",
            add: "",
            job:"",
            department:"",
            education:"",
            Career:"",


        }
        mageeapi.getjoindata().then(function(data) {
            vm.post = data.data.post.map(function(data) {
                return { id: data.area_uid, name: data.city_name + "/" + data.area_name };
            });
            vm.education=data.data.education;
            vm.job=data.data.job;
            vm.work_state=data.data.work_state;
        });
        // vm.post=[{id:235,name:'新北市 中和區'},{id:234,name:'新北市 永和區'}]
        vm.sign = function() {
        	var str=[];
			if(vm.member.member_name==""){
				str.push("請輸入您的姓名");
			}
			if(vm.member.phone=="" && vm.member.cellphone==""){
				str.push("請輸入您的電話或者行動電話(二擇一即可)");
			}
			if(vm.member.sex==""){
				str.push("請選擇您的性別");
			}
			if(str!=""){
				alert(str.join("\t\n"));
				return ;
			}
			if(vm.agree){
				mageeapi.joinmagee(vm.member).then(function (data){
					console.log(data)
				});

			}
            console.log(vm.member);
        }
    });
