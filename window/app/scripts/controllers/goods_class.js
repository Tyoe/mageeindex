'use strict';

/**
 * @ngdoc function
 * @name mageeWindowApp.controller:GoodsClassCtrl
 * @description
 * # GoodsClassCtrl
 * Controller of the mageeWindowApp
 */
angular.module('mageeWindowApp')
    .controller('GoodsClassCtrl', function($stateParams, Product, API_FILE, mageeapi) {
        var vm = this;
        // var tmptext = "<div class='text-hit'>最後3名，全部滿班！請速洽0227338118</div>\r保證班40H課程，再享40H課程回訓，含模擬考總複習+贈絕對考上套書+DVD雙函授課程+線上測驗 20回\r【好朋友兩人同行，再享推薦金500元】\r※報名課程再送免費導遊口試教學講座";
        mageeapi.getAd().then(function(data) {
            vm.ad = data.data.map(function(item) {
                item.image = (item.image) ? API_FILE + item.image : "";
                return $item;
            });
        });
        vm.class = "";
        var ks = function() {
            Product.getClassPageList().then(function(data) {
                vm.course_news = (data.data.page) ? API_FILE + data.data.page.context : "";
                vm.exam_news = (data.data.page) ? data.data.page.remark : "";

                vm.course_class = data.data.ProClass.map(function(item) {
                    item.color = API_FILE + "../" + item.color;
                    // var tmpitem = [];
                    // angular.forEach(data.data.WebClassPro, function(item) {
                    //     items.bimage = API_FILE + "../" + items.bimage;
                    //     tmpitem.push(items);
                    // });
                    // item.WebClassPro = tmpitem;
                    item.WebClassPro = item.WebClassPro.map(function(items) {
                        items.bimage = API_FILE + "../" + items.bimage;
                        // items.product.map(function ($iit){

                        // })
                        return items;
                    });
                    return item;
                });
            });
        }
        ks();

        vm.goto = function(data) {
            // $("body").animate({ scrollTop: $("#" + data).offset().top - 40 }, "slow");
            vm.class = data;
        }
    });
