'use strict';

/**
 * @ngdoc function
 * @name mageeWindowApp.controller:EditPasswordCtrl
 * @description
 * # EditPasswordCtrl
 * Controller of the mageeWindowApp
 */
angular.module('mageeWindowApp')
    .controller('EditPasswordCtrl', function($scope, Member) {
        var vm = this;
        Member.getmember(vm, $scope);
        vm.change = function() {
            if (vm.member.newpw == vm.member.repw ) {
                Member.editpwd({ data: vm.member }).then(function(data) {
                    console.log(data);
                    if(data.data.status=="OK"){
                        alert("密碼修改成功");
                        vm.member.newpw = vm.member.repw="";
                    }else if(data.data.status=="Passwordnochecked"){
                        alert("密碼與確認密碼不相符");
                    }
                })
            }else{
                 alert("密碼與確認密碼不相符");
            };
        }
    });
