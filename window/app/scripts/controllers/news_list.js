'use strict';

/**
 * @ngdoc function
 * @name mageeWindowApp.controller:NewsListCtrl
 * @description
 * # NewsListCtrl
 * Controller of the mageeWindowApp
 */
angular.module('mageeWindowApp')
    .controller('NewsListCtrl', function($stateParams, mageeapi, API_FILE) {
        var vm = this;
        vm.list_group = ($stateParams.group) ? $stateParams.group : "1";
        mageeapi.getDocList({ group: vm.list_group }).then(function(data) {
            vm.list_more = data.data.Group;
            vm.Docslist = data.data.Docslist.map(function(item) {
                return { id: item.id, name: item.name, abstract: item.content_s, img: (item.img) ? API_FILE + item.img : "" };
            });
        });

        // vm.list_more = [{
        //     id: "1",
        //     name: "應考須知",
        //     news: [{ id: 1, name: "106年度領隊、導遊人員證照考試", img: "images/goods-normal2.png", abstract: "◎ 報名日期：105年11月22日(二)~105年12月01日(四)\r◎ 考試日期：106年03月04日(六)~106年03月05日(日)" },
        //         { id: 2, name: "【應試資格】領隊 / 導遊證照", img: "images/goods-normal2.png", abstract: "榜首分享領隊導遊考試之準備考試技巧&捷徑\r放描述" },
        //         { id: 3, name: "【考試科目】/【及格標準】", img: "images/goods-normal2.png", abstract: "榜首分享領隊導遊考試之準備考試技巧&捷徑\r放描述" },
        //         { id: 4, name: "106年度領隊、導遊人員證照考試", img: "images/goods-normal2.png", abstract: "榜首分享領隊導遊考試之準備考試技巧&捷徑\r放描述" },
        //         { id: 5, name: "【應試資格】領隊 / 導遊證照", img: "images/goods-normal2.png", abstract: "榜首分享領隊導遊考試之準備考試技巧&捷徑\r放描述" },
        //         { id: 6, name: "【考試科目】/【及格標準】", img: "images/goods-normal2.png", abstract: "榜首分享領隊導遊考試之準備考試技巧&捷徑\r放描述" }
        //     ]
        // }, {
        //     id: "doc2",
        //     name: "考試分析",
        //     news: [{ id: 1, name: "考選部統計【考試錄取率】", img: "images/goods-normal2.png", abstract: "榜首分享領隊導遊考試之準備考試技巧&捷徑\r放描述" },
        //         { id: 2, name: "【認識領隊及導遊】", img: "images/goods-normal2.png", abstract: "榜首分享領隊導遊考試之準備考試技巧&捷徑\r放描述" }
        //     ]
        // }, {
        //     id: "doc3",
        //     name: "學員心得",
        //     news: [{ id: 1, name: "學員心得分享‧第 e 篇", img: "images/goods-normal2.png", abstract: "榜首分享領隊導遊考試之準備考試技巧&捷徑\r放描述" },
        //         { id: 2, name: "學員心得分享‧第 2 篇", img: "images/goods-normal2.png", abstract: "榜首分享領隊導遊考試之準備考試技巧&捷徑\r放描述" },
        //         { id: 3, name: "學員心得分享‧第 3 篇", img: "images/goods-normal2.png", abstract: "榜首分享領隊導遊考試之準備考試技巧&捷徑\r放描述" }
        //     ]
        // }, {
        //     id: "doc4",
        //     name: "馬跡品牌",
        //     news: [{ id: 1, name: "關於馬跡領對導遊訓練中心", img: "images/goods-normal2.png", abstract: "榜首分享領隊導遊考試之準備考試技巧&捷徑\r放描述" },
        //         { id: 2, name: "免費專業諮詢", img: "images/goods-normal.png", abstract: "榜首分享領隊導遊考試之準備考試技巧&捷徑\r放描述" },
        //         { id: 3, name: "超強師資", img: "images/goods-normal3.png", abstract: "榜首分享領隊導遊考試之準備考試技巧&捷徑\r放描述" }
        //     ]
        // }, {
        //     id: "doc5",
        //     name: "修正公告",
        //     news: [{ id: 1, name: "絕對考上導遊+領隊書籍【日語篇】修正公告", img: "", abstract: "榜首分享領隊導遊考試之準備考試技巧&捷徑\r放描述" },
        //         { id: 2, name: "絕對考上導遊+領隊書籍【五版】修正公告 II", img: "images/goods-normal2.png", abstract: "榜首分享領隊導遊考試之準備考試技巧&捷徑\r放描述" },
        //         { id: 3, name: "絕對考上導遊+領隊書籍【英語篇】修正公告", img: "images/goods-normal2.png", abstract: "榜首分享領隊導遊考試之準備考試技巧&捷徑\r放描述" }
        //     ]
        // }]
    });
