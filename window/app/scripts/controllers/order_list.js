'use strict';

/**
 * @ngdoc function
 * @name mageeWindowApp.controller:OrderListCtrl
 * @description
 * # OrderListCtrl
 * Controller of the mageeWindowApp
 */
angular.module('mageeWindowApp')
  .controller('OrderListCtrl', function () {
  	var vm=this;
	vm.orderlist=[{
					ordernum:"ORDER1703200001",paytotal:15500,status:"1",sentway:'郵局寄送',sentdate:'-',
				  	sentnum:'-',show:false,
				 	statuschange:[{num:1,date:"2016-08-24"},{num:2,date:"-"},{num:3,date:"-"},{num:4,date:"-"}],
				 	goods:[{name:"絕對考上 日語篇",price:300,quantity:2},
    			 		   {name:"帶團書",price:500,quantity:2}]

    			 },{
				  	ordernum:"ORDER1703200002",paytotal:15500,status:"4",sentway:'自取',sentdate:'2016-09-03',
				  	sentnum:'109485000112',show:false,
				  	statuschange:[{num:1,date:"2016-08-26"},{num:2,date:"2016-08-26"},{num:3,date:"2016-09-02"},{num:4,date:"2016-09-03"}],
				  	goods:[{name:"1/14領隊導遊證照班 - 台北班",price:13900,quantity:1},
    			 		   {name:"帶團書",price:500,quantity:2}]
    			 },{

    			 	ordernum:"ORDER1703200002",paytotal:15500,status:"3",sentway:'7-11店到店',sentdate:'-',
				  	sentnum:'-',show:false,
				  	statuschange:[{num:1,date:"2016-10-01"},{num:2,date:"2016-10-01"},{num:3,date:"2016-10-02"},{num:4,date:"-"}],
				  	goods:[{name:"帶團書",price:500,quantity:2}]}

				 ]

  vm.status=[{stepnum:1,name:"訂單成立"},{stepnum:2,name:"帳款確認中"},{stepnum:3,name:"商品包裝中"},{stepnum:4,name:"商品出貨"}]
  });
