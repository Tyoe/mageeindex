'use strict';

/**
 * @ngdoc function
 * @name mageeWindowApp.controller:HeaderHeaderCtrl
 * @description
 * # HeaderHeaderCtrl
 * Controller of the mageeWindowApp
 */
angular.module('mageeWindowApp')
    .controller('HeaderHeaderCtrl', function($auth, $uibModal, Member, $scope,mageeapi) {
        var vm = this;
        Member.getmember(vm,$scope);
        vm.login = function() {
            var open = $uibModal.open({
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'views/login/page.html',
                controller: 'LoginPageCtrl',
                controllerAs: 'lpc',
                backdrop: "static",
                size: "md",
            });
            open.result.then(function(data) {
                console.log($scope);
            });
        }
        mageeapi.getDocsGroupList().then(function (data){
            vm.grouplist=data.data;
            console.log(data);
        })
        // localStorage['api_toekn'];
    });
