'use strict';

/**
 * @ngdoc function
 * @name mageeWindowApp.controller:GoodsMoreCtrl
 * @description
 * # GoodsMoreCtrl
 * Controller of the mageeWindowApp
 */
angular.module('mageeWindowApp')
    .controller('GoodsMoreCtrl', function($uibModal, $stateParams, Product, API_FILE, $state, $filter) {
        var vm = this;
        vm.newcart = (localStorage["cart"]) ? JSON.parse(localStorage["cart"]) : [];
        vm.pro_classid = ($stateParams.proid) ? $stateParams.proid : "0";
        vm.pro_caseid = ($stateParams.caseid) ? $stateParams.caseid : "0";
        vm.CSorBK = (vm.pro_caseid == 0) ? "BK" : "CS";
        var tk = function() {
            Product.getpromore({
                classid: vm.pro_classid,
                caseid: vm.pro_caseid
            }).then(function(data) {
                // console.log(data);
                if (vm.CSorBK == "CS") {
                    vm.case = data.data.ProCase;
                    vm.ProName=data.data.ProName.name;
                }
                data.data.product.image = API_FILE + "../" + data.data.product.image;
                data.data.product.smalinfor = (data.data.product.smalinfor) ? API_FILE + data.data.product.smalinfor : null;
                data.data.product.webcontext = (data.data.product.webcontext) ? API_FILE + data.data.product.webcontext : null;
                vm.pro_detail = data.data.product;
                // vm.pro_detail = data.data
            });
        }
        tk();
        vm.test = function() {
            tk();
        }
        vm.incart = function() {
            var number = ($filter("filter")(vm.newcart, { id: vm.pro_classid })).length;
            if (number == 0) {
                vm.newcart.push({ "id": vm.pro_classid, "number": 1, kid: "main" });
                localStorage["cart"] = JSON.stringify(vm.newcart);
            }
            console.log(vm.newcart);
        }
        vm.gobuypage = function() {
            vm.incart();
            $state.go("shopcart");
        }

    });
