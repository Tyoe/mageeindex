'use strict';

/**
 * @ngdoc function
 * @name mageeWindowApp.controller:LoginPageCtrl
 * @description
 * # LoginPageCtrl
 * Controller of the mageeWindowApp
 */
angular.module('mageeWindowApp')
    .controller('LoginPageCtrl', function(httpctrl, $auth, $uibModalInstance, $state) {
        var vm = this;
        vm.submit = function() {
            $auth.login({ account: vm.account, password: vm.password }).then(function(data) {
                switch (data.data.status) {
                    case "open":
                        $auth.setToken(data.data.member.api_token);
                        $state.reload();
                        $uibModalInstance.close();
                        break;
                    case "no-open":
                        alert("您的會員尚未開通，請致電中心");
                        break;
                    case "nojoinmember":
                        alert("尚未加入會員");
                        break;

                }

            });

        }

    });
