'use strict';

/**
 * @ngdoc function
 * @name mageeWindowApp.controller:NewsCtrl
 * @description
 * # NewsCtrl
 * Controller of the mageeWindowApp
 */
angular.module('mageeWindowApp')
    .controller('NewsCtrl', function($stateParams, $location, $anchorScroll, mageeapi, API_FILE) {
        var vm = this;
        vm.goto = function(tag) {
            $location.hash(tag);
            $anchorScroll();
        };

        // vm.about = "views/demo/class_news.html"
        vm.list_group = ($stateParams.listid) ? $stateParams.listid : "";
        vm.list_doc = ($stateParams.docid) ? $stateParams.docid : "";
        mageeapi.getDocsInfor({ group: vm.list_group, doc: vm.list_doc }).then(function(data) {
            data.data.Docinfo.content = API_FILE + data.data.Docinfo.content
            vm.Group = data.data.Group;
            vm.DocsInfor = data.data.Docinfo;
            vm.DocTag = data.data.DocTag;
        });
        // vm.prevdocid = vm.list_doc - 1;
        // vm.nextdocid = vm.list_doc - 1 + 2;
        // vm.doc_detail = {
        //     docid: 1,
        //     docclass: 1,
        //     img: "images/goods-normal2.png",
        //     title_hit: '【NEW！考試公告】',
        //     title: "106年度領隊、導遊人員證照考試",
        //     abstract: "◎ 報名日期：105年11月22日(二)~105年12月01日(四)\r◎ 考試日期：106年03月04日(六)~106年03月05日(日)",
        //     textelement: [
        //         { id: "no1", name: "承辦單位", alltext: "專技考試司第一科\r試務處電話\r(02)2236-9188轉3926、3927請登入考選部全球資訊網，網址http://www.moex.gov.tw，點選網路報名後，即可進入網路報名資訊系統入口網站，或以網址http://register.moex.gov.tw 直接進行報名，並自行下載列印應考須知及報名書表。請登入考選部全球資訊網，網址http://www.moex.gov.tw，點選網路報名後，即可進入網路報名資訊系統入口網站，或以網址http://register.moex.gov.tw 直接進行報名，並自行下載列印應考須知及報名書表。請登入考選部全球資訊網，網址http://www.moex.gov.tw，點選網路報名後，即可進入網路報名資訊系統入口網站，或以網址http://register.moex.gov.tw 直接進行報名，並自行下載列印應考須知及報名書表。請登入考選部全球資訊網，網址http://www.moex.gov.tw，點選網路報名後，即可進入網路報名資訊系統入口網站，或以網址http://register.moex.gov.tw 直接進行報名，並自行下載列印應考須知及報名書表。" },
        //         { id: "no2", name: "考試公告", alltext: "專技考試司第一科\r試務處電話\r(02)2236-9188轉3926、3927" },
        //         { id: "no3", name: "考試類科", alltext: "專技考試司第一科\r試務處電話\r(02)2236-9188轉3926、3927" },
        //         { id: "no4", name: "考試日期", alltext: "專技考試司第一科\r試務處電話\r(02)2236-9188轉3926、3927" },
        //         { id: "no5", name: "考試地點", alltext: "專技考試司第一科\r試務處電話\r(02)2236-9188轉3926、3927" },
        //         { id: "no6", name: "報名方式", alltext: "專技考試司第一科\r試務處電話\r(02)2236-9188轉3926、3927" },
        //         { id: "no7", name: "放榜日期", alltext: "專技考試司第一科\r試務處電話\r(02)2236-9188轉3926、3927" },
        //         { id: "no8", name: "日程表", alltext: "專技考試司第一科\r試務處電話\r(02)2236-9188轉3926、3927" }
        //     ],
        //     content_count: 8
        // };
        // vm.prevdoc = { docid: 1, title: "【應試資格】領隊 / 導遊證照" }
        // vm.nextdoc = { docid: 2, title: "【考試科目】/【及格標準】" }
        // vm.random = { class: 3, docid: 2, title: "快速考取證照的方法" }
    });
