'use strict';

/**
 * @ngdoc function
 * @name mageeWindowApp.controller:PersonDataCtrl
 * @description
 * # PersonDataCtrl
 * Controller of the mageeWindowApp
 */
angular.module('mageeWindowApp')
  .controller('PersonDataCtrl', function (Member) {
  	var vm=this;
  	Member.getmemberinfo().then(function (data){
  		console.log(data);
  		vm.data=data.data;
  	});
  	
  });
