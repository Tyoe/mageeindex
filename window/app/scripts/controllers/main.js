'use strict';

/**
 * @ngdoc function
 * @name mageeWindowApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the mageeWindowApp
 */

angular.module('mageeWindowApp')
    .controller('MainCtrl', function($scope, API_FILE, $window, $rootScope, mageeapi, $filter) {
        var vm = this;
        vm.Interval = 5000;
        vm.contentGroup = [{
            content: [
                { name: "載入中", content: "loading..." },
                { name: "載入中", content: "loading..." }
            ]
        }];
        //try
        $(window).resize(function() {
            vm.wdth=$(window).width();
        });
        vm.Active_content = 0;
        mageeapi.getIndex().then(function(data) {
            console.log(data);
            vm.news = data.data.News;
            vm.banner = data.data.Banner.map(function(item) {
                item.image = (item.image) ? API_FILE + "../" + item.image : "";
                return item;
            });
            data.data.Content = data.data.Content.map(function(item) {
                item.image = (item.image) ? API_FILE + "../" + item.image : "";
                item.content = (item.content) ? API_FILE + item.content : "";
                return item;
            });
            vm.WebFunclist = data.data.WebFunclist.map(function(item) {
                item.image = (item.image) ? API_FILE + "../" + item.image : "";
                return item;
            });
            console.log(vm.WebFunclist);
            var list = $filter("modgroup")(data.data.Content, { index: "content", number: 2 });
            vm.contentGroup = list;
        });
        mageeapi.getAd().then(function(data) {
            vm.ad = data.data;
        });
    });
