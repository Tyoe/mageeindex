'use strict';

/**
 * @ngdoc function
 * @name mageeWindowApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the mageeWindowApp
 */
angular.module('mageeWindowApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
