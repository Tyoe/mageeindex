'use strict';

/**
 * @ngdoc service
 * @name mageeWindowApp.httpctrl
 * @description
 * # httpctrl
 * Factory in the mageeWindowApp.
 */
angular.module('mageeWindowApp')
    .factory('httpctrl', function(API, $http,fromgetdata) {
        var httpget = function(url, dafult) {
            dafult = (dafult) ? dafult : {};
            return $http.get(API + url + fromgetdata(dafult));
        }
        var httppost = function(url, dafult, header) {
            dafult = (dafult) ? dafult : {};
            return $http.post(API + url, dafult, header);
        }
        return {
            get: httpget,
            post: httppost
        };
    });
