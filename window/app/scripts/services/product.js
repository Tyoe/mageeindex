'use strict';

/**
 * @ngdoc service
 * @name mageeWindowApp.Product
 * @description
 * # Product
 * Factory in the mageeWindowApp.
 */
angular.module('mageeWindowApp')
    .factory('Product', function(httpctrl) {
        // Service logic
        // ...

        var getClassPageList = function() {
            return httpctrl.get("getClassPageList");
        };
        var getpromore=function (data){
          return httpctrl.get("getpromore",data);
        }
         var getBookPageList = function() {
            return httpctrl.get("getBookPageList");
        };
        var getCartPro=function (data){
            return httpctrl.post("getCartPro",data);
        }

        // Public API here
        return {
            getClassPageList: getClassPageList,
            getpromore:getpromore,
            getBookPageList:getBookPageList,
            getCartPro:getCartPro
        };
    });
