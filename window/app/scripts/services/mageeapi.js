'use strict';

/**
 * @ngdoc service
 * @name mageeWindowApp.mageeapi
 * @description
 * # mageeapi
 * Factory in the mageeWindowApp.
 */
angular.module('mageeWindowApp')
    .factory('mageeapi', function(httpctrl) {
        // Service logic
        // ...
        var getDoclist = function() {
            return httpctrl.get("getDoclist");
        }
        var getpost = function() {
            return httpctrl.get("getpost");
        }
        var changedelvary=function (data){
            return httpctrl.post("changedelvary",data);
        }
        var submitorder=function (data){
            return httpctrl.post("submitorder",data);
        }
        var getDocsGroupList=function (data){
            return httpctrl.get("getDocsGroupList");
        }
        var getDocList=function (data){
            return httpctrl.get("getDocList",data);
        }
        var getDocsInfor=function (data){
            return httpctrl.get("getDocsInfor",data);
        }
        var getIndex=function (){
            return httpctrl.get("getIndex");
        }
        var getAd=function (){
            return httpctrl.get("getAd");
        }
        var joinmagee=function (data){
            return httpctrl.post("joinmagee",data);
        }
        var getjoindata=function (){
            return httpctrl.get("getjoindata");
        }
        // Public API here
        return {
            getDoclist: getDoclist,
            getpost: getpost,
            changedelvary:changedelvary,
            submitorder:submitorder,
            getDocsGroupList:getDocsGroupList,
            getDocList:getDocList,
            getDocsInfor:getDocsInfor,
            getIndex:getIndex,
            getAd:getAd,
            joinmagee:joinmagee,
            getjoindata:getjoindata
        };
    });
