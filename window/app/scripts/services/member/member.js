'use strict';

/**
 * @ngdoc service
 * @name mageeWindowApp.Member/member
 * @description
 * # Member/member
 * Factory in the mageeWindowApp.
 */
angular.module('mageeWindowApp')
    .factory('Member', function(httpctrl) {
        var checkmember = function() {
            return httpctrl.get("getmember");
        }
        var getmember = function(vm,$scope) {
            $scope.$watch(function() {
                return $scope.$root.member
            }, function(data) {
                if (data) {
                    var member = $scope.$root.member;
                    vm.member = member;
                }else{
                    vm.member=data;
                }
            });
        }
        var editpwd=function (data){
            return httpctrl.post("editpwd",data);
        }
        var getmemberinfo=function (){
            return httpctrl.get("getmemberinfo");
        }
        // Public API here
        return {
            checkmember: checkmember,
            getmember:getmember,
            editpwd:editpwd,
            getmemberinfo:getmemberinfo
        };
    });
