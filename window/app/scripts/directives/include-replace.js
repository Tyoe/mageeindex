'use strict';

/**
 * @ngdoc directive
 * @name mageeWindowApp.directive:includeReplace
 * @description
 * # includeReplace
 */
angular.module('mageeWindowApp')
    .directive('includeReplace', function() {
        return {
            require: 'ngInclude',
            restrict: 'A',
            /* optional */
            link: function(scope, el, attrs) {
                el.replaceWith(el.children());
            }
        };
    });
