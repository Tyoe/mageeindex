'use strict';

/**
 * @ngdoc directive
 * @name mageeWindowApp.directive:header/header
 * @description
 * # header/header
 */
angular.module('mageeWindowApp')
    .directive('headerInfor', function() {
        return {
            templateUrl: "views/header/header.html",
            restrict: 'AE',
            controller: "HeaderHeaderCtrl",
            controllerAs: "hhc",
            link: function postLink(scope, element, attrs) {

            }
        };
    });
