'use strict';

/**
 * @ngdoc directive
 * @name mageeWindowApp.directive:backtotop
 * @description
 * # backtotop
 */
angular.module('mageeWindowApp')
  .directive('backtotop', function () {
    return {
      restrict: 'A',
      link: function postLink(scope, element, attrs) {
        var topbtn = $('.gotop');
        $(window).scroll(function () {
          if ($(document).scrollTop() > 100){
            topbtn.fadeIn(500)
          }else{
            topbtn.fadeOut(500);
          }
        });
        element.on("click",function(){
        	$("html,body").animate({"scrollTop":0},500);
        })
      }
    };
  });
