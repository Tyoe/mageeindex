'use strict';

/**
 * @ngdoc directive
 * @name mageeWindowApp.directive:lightshow
 * @description
 * # lightshow
 */
angular.module('mageeWindowApp')
  .directive('lightshow', function () {
    return {
      restrict: 'AE',
      link: function postLink(scope, element, attrs) {
      	console.log(element.find(".carousel-inner>.item").length)
      }
    };
  });
