'use strict';

/**
 * @ngdoc directive
 * @name mageeWindowApp.directive:trantextarea
 * @description
 * # trantextarea
 */
angular.module('mageeWindowApp')
  .directive('trantextarea', function () {
    return {
      template: '<div></div>',
      restrict: 'AE',
      link: function postLink(scope, element, attrs) {
        element.text('this is the trantextarea directive');
      }
    };
  });
